'use strict';

module.exports = function (Payment) {
    
    const Ajv = require('ajv');

    const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

    function callbackWithError(errorDescription, errorCode, callback) {
        var error = new Error(errorDescription);
        error.status = errorCode;
        callback(error);
    }


    Payment.purchase = function(options, params, callback){

        const Profile = Payment.app.models.Profile;

        Profile.findOne({
            where : {
                userId : options.accessToken.userId
            }
        },function(err,profileResult){
            if(err){
                callback(err);
                return;
            }

            if(!profileResult){
                callbackWithError("NotExists",404,callback);
                return;
            }
            

            console.log("params ==== ",params);

            var ajv = new Ajv({
                $data: true
            });
            var schema = {
                "type": "object",
                "properties": {
                    "tokensCount": {
                        "type": "number"
                    },
                    "token": {
                        "type": "string"
                    }
                },
                "additionalProperties": false,
                "required": ["token","tokensCount"] 
            };
    
            var validate = ajv.compile(schema);
    
            if (!validate(params)) {
                var error = new Error("Format invalide");
                error.status = 400;
                callback(error);
                return;
            }
    
    
            stripe.charges
                .create({
                  amount: (params.tokensCount == 12 ? 10 : 1) * 100, // Unit: cents
                  currency: 'eur',
                  source: params.token,
                  description: "Achat jetons",
                  receipt_email: 'salimbrahmi11@gmail.com',
                })
                .then(result => {
                    console.log("stripe result ==== ",result);
    
                    result = JSON.parse(JSON.stringify(result));
    
                    const purchaseParams = {
                        userId : options.accessToken.userId,
                        stripeId : result.id 
                    }
    
                    Payment.create(purchaseParams,function(err,purchaseResult){});
    
                    profileResult.updateAttribute("tokensCount",profileResult.tokensCount + params.tokensCount,function(err,updateResult){
                        if(err){
                            callback(err);
                            return;
                        }
    
                        callback(null);
                    });
                });
        });
    };

}