'use strict';

module.exports = function (Address) {

    function callbackWithError(errorDescription, errorCode, callback) {
        var error = new Error(errorDescription);
        error.status = errorCode;
        callback(error);
    }

    Address.edit =  function(addressId,params,tx,callback){

        Address.findOne({
            where : {
                addressId:addressId
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            if(result == null){
                callbackWithError("NotExists",404,callback);
                return;
            }

            result.updateAttributes(params,{transaction : tx},function(err,result){
                if(err){
                    callback(err);
                    return;
                }

                callback(null);
            });
        })
    }
}