'use strict';

module.exports = function (Member) {

  const isJSON = require('is-json');
  const Ajv = require('ajv');

  function callbackWithError(errorDescription, errorCode, callback) {
    var error = new Error(errorDescription);
    error.status = errorCode;
    callback(error);
  }

  function generateCode() {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 8; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  Member.register = function (ctx, callback) {
    const asset = Member.app.models.Asset;
    ctx.req.params.container = 'profile';

    asset.uploadMultiple(ctx, null, function (err, uploadResult, fileObject) {
      if (err) {
        callback(err);
        asset.removeAssets(uploadResult, function (err, res) {});
        return;
      }

      const User = Member.app.models.user;

      User.beginTransaction('READ COMMITTED', function (err, tx) {

        console.log("[transaction ", tx.id, "] started");

        Member.saveMemberInfos(uploadResult, fileObject, tx, function (err, userCredentials) {
          if (err) {
            tx.rollback(function (error) {
              asset.removeAssets(uploadResult, function (err, res) {});
              console.log("[transaction ", tx.id, "] rolledback");
              callback(err);
            });
            return;
          }

          tx.commit(function (error) {
            console.log("[transaction ", tx.id, "] commited");
            User.auth(userCredentials,true,function(err,result){
                if(err){
                    console.log(err);
                    callback(null);
                    return;
                }
                const Category = Member.app.models.Category;

                let options = {
                  accessToken : {
                    userId : result.token.userId
                  }
                };
                
                Member.sendEmailVerification(result.token.id,result.user.email);

                Category.allCategories (options,function(err,categoriesResult){
                    result.Categories = categoriesResult;
                    callback(null, result);
                });

            });
          });
        });
      });
    });
  }

  Member.saveMemberInfos = function (uploadResult, fileObject, tx, callback) {

    console.log("uploadResult === ", uploadResult);
    console.log("fileObject === ", fileObject);

    const asset = Member.app.models.Asset;

    if (!fileObject.fields['data'] || !fileObject.fields['data'][0]) {
      callbackWithError("data requise", 400, callback);
      return;
    }

    let params = fileObject.fields['data'][0];

    if (!isJSON(params)) {
      callbackWithError("Format d'objet invalide", 400, callback);
      return;
    }

    params = JSON.parse(params);

    var ajv = new Ajv({
      $data: true
    });

    var schema = {
      "type": "object",
      "properties": {
        "User": {
          "type": "object",
          "properties": {
            "email": {
              "type": "string",
              "format": "email"
            },
            "password": {
              "type": "string",
            }
          },
          "required": ['email', 'password'],
          "additionalProperties": false
        },
        "Profile": {
          "type": "object",
          "properties": {
            "firstName": {
              "type": "string",
            },
            "lastName": {
              "type": "string",
            },
            "birthdate": {
              "type": "string",
            },
            "isMale": {
              "type": "boolean",
            },
            "code": {
              "type": "string",
            }
          },
          "required": ['firstName', 'isMale'],
          "additionalProperties": false
        }
      },
      "required": ['User', 'Profile'],
      "additionalProperties": false
    }

    var validate = ajv.compile(schema);

    if (!validate(params)) {
      callbackWithError("Format invalide", 400, callback);
      return;
    }

    const User = Member.app.models.user;
    const Profile = Member.app.models.Profile;
    const role = Member.app.models.Role;
    const RoleMapping = Member.app.models.RoleMapping;

    if(params.Profile && params.Profile.code){

      Profile.findOne({
        where : {
          sponsorshipCode : params.Profile.code
        }
      },function(err,sponsorUserResult){
        if(err){
          callback(err);
          return;
        }

        if(!sponsorUserResult){
          callbackWithError("Code de parrainage invalide",408,callback);
          return;
        }


        params.Profile.sponsoredBy = sponsorUserResult.userId;
        proceed(sponsorUserResult);
      });

      return;
    }

    proceed(null);

    function proceed(sponsorUserResult){
      User.create(params.User, {
        transaction: tx
      }, function (err, userResult) {
        if (err) {
          callback(err);
          return;
        }
  
        role.findOne({
            where: {
              name: "MEMBER"
            }
          },
          function (error, roleRes) {
            if (error) {
              console.log(error);
              callback(err);
              return;
            }
            // MAP ROLE  
            roleRes.principals.create({
              principalType: RoleMapping.USER,
              principalId: userResult.id,
              roleId: roleRes.id
            }, {
              transaction: tx
            }, function (err, principal) {
              if (err) {
                callback(err);
                return;
              }
  
              params.Profile.userId = userResult.id;
              params.Profile.sponsorshipCode = generateCode();
  
              if (uploadResult != null && uploadResult.length > 0 && uploadResult[0].for == "profile") {
                params.Profile.avatarId = uploadResult[0].assetId;
              }
  
              Profile.create(params.Profile, {
                transaction: tx
              }, function (err, profileResult) {
                if (err) {
                  callback(err);
                  return;
                }

                if(sponsorUserResult){
                  sponsorUserResult.updateAttribute("tokensCount",sponsorUserResult.tokensCount + 1,function(err){
                    if(err){
                      console.log("update sponsor user error ===",err);
                    }
                  });
                }
  
                callback(null,params.User);
              });
            });
          });
      });
    }

  }

  Member.sendEmailVerification = function(token,email){
  
      let url = process.env.BACKOFFICE_URL+'verify-email/'+ token;
  
  
      var html = "<div style='margin: 3%; padding: 2%; text-align: center; border: 1px solid transparent; border-radius: 10px; background-color: #f3f3f3;'>" +
        "<p style = 'color:black;'>Vérifie ton adresse e-mail pour pouvoir accèder à ton compte Yepwego</p>" +
        "<a style='text-decoration: none; padding : 8px; color:white; text-align: center; border-radius: 5px; background-color: #49a0da;' href='" + url + "'>Vérifier</a>" +
        "</div>";
  
      var mailjet = require('node-mailjet')
         .connect(process.env.MAILJET_API_KEY, process.env.MAILJET_SECRET_KEY);
      var request = mailjet
        .post("send")
        .request({
          "FromEmail": process.env.EMAIL_SENDER,
          "FromName": process.env.FROM_EMAIL_NAME,
          "Subject": "Vérifier ton adresse e-mail",
          "Html-part": html,
          "Recipients": [{
            "Email": email
          }]
        });
      request.then((result) => {
          console.log(result.body)
        })
        .catch((err) => {
          console.log(err)
      });
  }

  Member.saveSponsorshipCode = function(options,params,callback){

    const Profile = Member.app.models.Profile;
    
    Profile.findOne({
      where : {
        sponsorshipCode : params.sponsorshipCode
      }
    },function(err,sponsorUserResult){
      if(err){
        callback(err);
        return;
      }

      if(!sponsorUserResult){
        callbackWithError("Code de parrainage invalide",408,callback);
        return;
      }

      Profile.findOne({
        where : {
          userId : options.accessToken.userId
        }
      },function(err,profileResult){
        if(err){
          callback(err);
          return;
        }


        profileResult.updateAttribute("sponsoredBy",sponsorUserResult.userId,function(err,result){
          if(err){
            callback(err);
            return;
          }

          sponsorUserResult.updateAttribute("tokensCount",sponsorUserResult.tokensCount + 1,function(err){
            if(err){
              console.log("update sponsor user error ===",err);
            }
          });

          callback(null);
        });
      });
    });
  }
}
