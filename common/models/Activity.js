'use strict';

module.exports = function (Activity) {

    function callbackWithError(errorDescription, errorCode, callback) {
        var error = new Error(errorDescription);
        error.status = errorCode;
        callback(error);
    }

    function distance(lat1, lon1, lat2, lon2, unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            var radlat1 = Math.PI * lat1/180;
            var radlat2 = Math.PI * lat2/180;
            var theta = lon1-lon2;
            var radtheta = Math.PI * theta/180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180/Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit=="K") { dist = dist * 1.609344 }
            if (unit=="N") { dist = dist * 0.8684 }
            return dist;
        }
    }

    Activity.aroundMe = function(options,lat,long,fromDate,toDate,categories,callback){

        var moment = require('moment');

        function compare(activity1,activity2) {  

            const firstDistance = distance(lat,long,parseFloat(activity1.Address.lattitude) ,parseFloat(activity1.Address.longitude));
            const secondDistance = distance(lat,long,parseFloat(activity2.Address.lattitude),parseFloat(activity2.Address.longitude));
    
            if (firstDistance < secondDistance)
              return -1;
            if (firstDistance > secondDistance)
              return 1;
            return 0;
        }

        function compareCategories(category1,category2) {  
            
        const firstDistance = distance(lat,long,parseFloat(category1.Activities[0].Address.lattitude) ,parseFloat(category1.Activities[0].Address.longitude));
        const secondDistance = distance(lat,long,parseFloat(category2.Activities[0].Address.lattitude),parseFloat(category2.Activities[0].Address.longitude));

        if (firstDistance < secondDistance)
            return -1;
        if (firstDistance > secondDistance)
            return 1;
        return 0;
        }
          
        const Category = Activity.app.models.Category;
        const SelectedCategory = Activity.app.models.SelectedCategory;
        const BlockedUser = Activity.app.models.BlockedUser;

        BlockedUser.blockedUsers(options,function(err,blockedUsersResult){
            if(err){
                callback(err);
                return;
            }

            SelectedCategory.find({
                where : {
                    userId : options.accessToken.userId,
                }
            },function(err,SelectedCatResult){
                if(err){
                    callback(err);
                    return;
                }
    
                SelectedCatResult = JSON.parse(JSON.stringify(SelectedCatResult));
    
    
                console.log("SelectedCatResult === ",SelectedCatResult);
    
    
                Category.allCategories(options,function(err,categoriesResult){
                    if(err){
                        callback(err);
                        return;
                    }
    
                    //search categories
                    if(categories && categories.length){
                        categoriesResult = categoriesResult.filter(category =>  categories.includes(category.categoryId));
                    }
    
                    // search with date
                    if(fromDate || toDate){
                        const fromdate = moment(fromDate, "YYYY-MM-DD");
                        const todate = moment(toDate, "YYYY-MM-DD").add(1, 'days');
                        categoriesResult.forEach(category => {
                            category.Activities = category.Activities.filter(activity => toDate ? moment(activity.startAt, "YYYY-MM-DD").isBetween(fromdate,todate,'days','[]')  : moment(activity.startAt,"YYYY-MM-DD").isSameOrAfter(fromdate))
                        });
                    }else{
    
                        //get only current activities
                        const now = moment().format("YYYY-MM-DD");
                        categoriesResult.forEach(category => {
                            category.Activities = category.Activities.filter(activity => moment(activity.startAt,"YYYY-MM-DD").isSameOrAfter(now))
                        });
    
                    }
    
                    //filter with distance
                    categoriesResult.forEach(category => {
                        category.Activities = category.Activities.filter(activity => {
                            const userDist = distance(lat,long,parseFloat(activity.Address.lattitude) ,parseFloat(activity.Address.longitude));
                            return userDist <= 50;
                        });
                    });
                    
    
                    //get public categories
                    categoriesResult.forEach(category => {
                        category.Activities = category.Activities.filter(activity => 
                            (activity.isPublic && activity.userId != options.accessToken.userId) && 
                            (blockedUsersResult.find(BlockedRelation => (BlockedRelation.userId == options.accessToken.userId && BlockedRelation.userToBlockId ==  activity.userId ) || (BlockedRelation.userToBlockId == options.accessToken.userId && BlockedRelation.userId ==  activity.userId ))) == null);
                    });
    
                    //get only selected categories
                    if(categoriesResult.filter(category => category.selected == true && category.Activities.length).length){
    
                        categoriesResult = categoriesResult.filter(category => category.selected == true );
    
                        //filter activities with level
                        let categoriesCopy = JSON.parse(JSON.stringify(categoriesResult));
                        categoriesCopy.forEach(category => {
                            let userLevel = SelectedCatResult.find(selected => selected.categoryId == category.categoryId);
                            
                            console.log('min level ==== ',userLevel);
                            if(userLevel.levelId){
                                category.Activities = category.Activities.filter(activity => activity.searchedLevel <= userLevel.levelId);
                            }
                        });
    
                        console.log("categoriesCopy === ",categoriesCopy);
                        // check if not from search and categories result have activities
                        if(!fromDate && categoriesCopy.filter(category => category.Activities.length).length){
                            categoriesResult = categoriesCopy;
                        }
    
                    }
    
                    //get categories with activities
                    categoriesResult = categoriesResult.filter(category => category.Activities.length);
    
    
                    //sort activities with distance
                    categoriesResult.forEach(category => {
                    if(category.Activities.length) category.Activities.sort(compare);
                    });
    
                    //sort categories with distance
                    categoriesResult.sort(compareCategories);

                    //new Latlong



                    let sameActivitiesPositions = [];
                    
                    categoriesResult.forEach(category => {
                        sameActivitiesPositions = sameActivitiesPositions.concat(category.Activities);
                    });

                    sameActivitiesPositions = sameActivitiesPositions.filter(activity => 
                        sameActivitiesPositions.find(act => 
                            act.Address.lattitude == activity.Address.lattitude &&
                              act.Address.longitude == activity.Address.longitude &&
                                activity.activityId != act.activityId));


                    sameActivitiesPositions.forEach((activity,index) => {

                        const dLat = (index - 1) * 5/6378137

                        activity.Address.lattitude = parseFloat(activity.Address.lattitude) + dLat * 180/3.14;


                        const dLon = (index - 1) * 5/(6378137*Math.cos(3.14*activity.Address.lattitude /180));

                        activity.Address.longitude = parseFloat(activity.Address.longitude) + dLon * 180/3.14 ;

                    });

                    categoriesResult.forEach(category => {
                        category.Activities.forEach(activity => {
                            const sameAct = sameActivitiesPositions.find(act => act.activityId == activity.activityId);
                            if(sameAct){
                                activity.Address.lattitude = sameAct.Address.lattitude ;
                                activity.Address.longitude = sameAct.Address.longitude ;
                            }
                        });
                    });


                        // console.log("sameActivitiesPositions === ",sameActivitiesPositions);

                    // console.log("categories === ",categoriesResult);
    
                    callback(null,categoriesResult);
                });
            });

        });
    }
    
    Activity.new = function(options,params,callback){

        const Profile = Activity.app.models.Profile;

        Profile.findOne({
            where : {
                userId : options.accessToken.userId 
            }
        },function(err,profileResult){
            if(err){
                callback(err);
                return;
            }

            if(!profileResult){
                callbackWithError("NotExists",404,callback);
                return;
            }

            if(!params.isPublic && !profileResult.tokensCount){
                callbackWithError("Tu n'as pas assez de jetons",409,callback);
                return;
            }

            Activity.beginTransaction('READ COMMITTED', function (err, tx) {
                const Address = Activity.app.models.Address;
    
                let addresses = [params.Address];
                params.MeetingPoint && addresses.push(params.MeetingPoint);
    
                console.log("addresses ==== ",addresses);
    
                Address.create(addresses,{transaction : tx},function(err,addressesResult){
                    if(err){
                        console.log("err ==== ",err);
                        tx.rollback(function (error) {
                            callback(err);
                        });
                        return;
                    }
    
                    console.log("addressesResult ==== ",addressesResult);
    
                    addressesResult.forEach(LocalAddress => {
                        var address = addresses.find(addr => addr.fullAddress == LocalAddress.fullAddress);
                        console.log("addres = ==== ",address)
                        if(address.isAddress){
                            params.addressId = LocalAddress.addressId;
                        }else{
                            params.meetingPointId = LocalAddress.addressId;
                        }
                    });
    
                    params.userId = options.accessToken.userId;
    
    
                    console.log("params ==== ",params);
    
                    Activity.create(params,{transaction : tx},function(err,result){
                        if(err){
                            tx.rollback(function (error) {
                                callback(err);
                            });
                            return;
                        }
    
                        const FollowRequest = Activity.app.models.FollowRequest;
                        const InvitationRequest = Activity.app.models.InvitationRequest;
    
                        FollowRequest.myFollowers(options,null,function(err,followers){
                            if(err){
                                tx.rollback(function (error) {
                                    callback(err);
                                });
                                return;
                            }
    
                            console.log("followerIds === ",followers);
    
                            tx.commit(function (error) {
                               
                                if(!params.isPublic ){
                                    profileResult.updateAttribute("tokensCount",profileResult.tokensCount - 1,function(err,updateResult){});
                                }

                                Activity.activityById(null,result.activityId,function(err,activityResult){
                                    if(err){
                                        console.log("err === ",err);
                                        callback(null);
                                        return;
                                    }
        
                                    callback(null,activityResult);
    
                                    if(followers.length && activityResult.isPublic){
                                        let followerIds = [];
            
                                        followers.forEach(follower => {
                                            followerIds.push(follower.followerId);
                                        });
                                        
                                        InvitationRequest.shareActivity(options, result.activityId,followerIds,function(err,inviteResult){});
                                    }
    
                                });
                            });
                        });
        
                    });
    
                });
                
            });
        });


        
    }

    Activity.edit = function(options,activityId,params,callback){


        console.log("edit activity params ===== ",params);

        Activity.findOne({
            where : {
                activityId : activityId,
                userId : options.accessToken.userId
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            if(result == null){
                callbackWithError("NotExists",404,callback);
                return;
            }

            Activity.beginTransaction('READ COMMITTED', function (err, tx) {
                if(params.MeetingPoint != null){
                    const Address = Activity.app.models.Address;
    
                    if(result.meetingPointId){
                        Address.edit(result.meetingPointId,params.MeetingPoint,tx,function(err){
                            if(err){
                                tx.rollback(function (error) {
                                    callback(err);
                                });
                                return;
                            }
    
                            edit(params,tx)
                        })
                    }else{
                        Address.create(params.MeetingPoint,{transaction : tx},function(err,addressesResult){
                            if(err){
                                tx.rollback(function (error) {
                                    callback(err);
                                });
                                return;
                            }

                            params.meetingPointId = addressesResult.addressId;
                            edit(params,tx);
                        });
                    }

                }else{
                    edit(params,tx);
                }
            });

            function edit(params,tx){
                result.updateAttributes(params,{transaction : tx},function(err,updateResult){
                    if(err){
                        tx.rollback(function (error) {
                            callback(err);
                        });
                        return;
                    }
    
                    tx.commit(function (error) {
                        Activity.activityById(null,activityId,function(err,activityResult){
                            if(err){
                                callback(null);
                                return;
                            }
        
                            callback(null,activityResult);
        
                        });
                    });
                });
            }
            
        });
    }

    Activity.removeActivity = function(options,activityId,callback){

        Activity.findOne({
            where : {
                activityId : activityId,
                userId : options.accessToken.userId
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            if(result == null){
                callbackWithError("NotExists",404,callback);
                return;
            }

            Activity.destroyById(activityId,function(err){
                if(err){
                    callback(err);
                    return;
                }

                callback(null);
            });
        });
    }

    Activity.activityById = function(options,activityId,callback){
        Activity.findOne({
            where : {
                activityId : activityId
            },
            include:[
                {
                    relation:'Category',
                    scope:{
                        include : [{
                            relation : "Cover"
                        },{
                            relation:"Activities"
                        }]
                    }
                },
                {
                  relation : "Participants",
                  scope : {
                    where : {
                      invitationStateId : 2
                    },
                    include : {
                      relation : "Invited",
                      scope : {
                          include : {
                              relation : "Avatar"
                          }
                      }
                    }
                  }
                },
                {
                  relation : "RequestsToParticipate",
                  scope : {
                    where : {
                      invitationStateId : 5
                    }
                  }
                },
                {
                    relation : "Address",
                },
                {
                    relation : "MeetingPoint",
                },
                {
                    relation:"Creator",
                    scope : {
                        include : [
                            {
                                relation : "Avatar"
                            },
                            {
                                relation :'Categories',
                                scope:{
                                    include : {
                                        relation :"Level"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    relation :"SearchedLevel"
                },
                {
                    relation :"Pictures"
                },
                {
                    relation :"ActivityPictures"
                },
                {
                    relation :"Comments",
                    scope:{
                        include :{
                            relation : "Commentator",
                            scope : {
                                include : {
                                    relation :"Avatar"
                                }
                            }
                        }
                    }
                }
            ]
        },function(err,activityResult){
            if(err){
                callback(err);
                return;
            }

            if(activityResult == null){
                callbackWithError("NotExists",404,callback);
                return;
            }

            const BlockedUser = Activity.app.models.BlockedUser;

            if(options){
                BlockedUser.blockedUsers(options,function(err,blockedUsersResult){
                    if(err){
                        callback(err);
                        return;
                    }

                    console.log("entered with blocked users ==== ",options.accessToken.userId);

                    proceed(JSON.parse(JSON.stringify(blockedUsersResult)) );
    
                });
            }else{
                proceed(null);
            }

            function proceed(blockedUsers){
                activityResult = JSON.parse(JSON.stringify(activityResult));

                if(activityResult && activityResult.Category &&  activityResult.Category.Cover){
                    activityResult.Category.coverURL = "storage/"+activityResult.Category.Cover.container+"/download/" + activityResult.Category.Cover.name;
                }
    
                if(activityResult && activityResult.Creator &&  activityResult.Creator.Avatar){
                    activityResult.Creator.avatarURL = "storage/"+activityResult.Creator.Avatar.container+"/download/" + activityResult.Creator.Avatar.name;
                }
    
                if(blockedUsers && blockedUsers.length)  
                    activityResult.ActivityPictures = activityResult.ActivityPictures.filter(picture =>  
                        blockedUsers.find(blockedUser =>  
                            ( blockedUser.userId == options.accessToken.userId && blockedUser.userToBlockId == picture.addedBy) ||
                             (blockedUser.userToBlockId == options.accessToken.userId && blockedUser.userId == picture.addedBy)) == null);

                  
                activityResult.Pictures = activityResult.Pictures.filter(picture => activityResult.ActivityPictures.find(actPic => actPic.assetId  == picture.assetId));
                
                activityResult.Pictures.forEach(picture => {
                    picture.url = "storage/"+picture.container+"/download/" + picture.name;
    
                }); 
    
                if(blockedUsers && blockedUsers.length) 
                    activityResult.Comments = activityResult.Comments.filter(comment => 
                        blockedUsers.find(blockedUser =>  
                            ( blockedUser.userId == options.accessToken.userId && blockedUser.userToBlockId == comment.userId) ||
                             (blockedUser.userToBlockId == options.accessToken.userId && blockedUser.userId == comment.userId)) == null);

                activityResult.Comments.forEach(comment => {
    
                    if(comment.Commentator && comment.Commentator.Avatar ){
                        comment.Commentator.avatarURL = "storage/"+comment.Commentator.Avatar.container+"/download/" + comment.Commentator.Avatar.name;
                    }
    
                }); 
    
                activityResult.Creator && activityResult.Creator.Categories.map(selectedCategory => {
                    if(selectedCategory.categoryId == activityResult.categoryId){
                        activityResult.organiserLevel =  selectedCategory.Level;
                    }
                });


                if(blockedUsers && blockedUsers.length) 
                    activityResult.Participants = activityResult.Participants.filter(request => 
                        request.Invited && 
                        !blockedUsers.includes(blockedUser => blockedUser.userId == request.Invited.userId || blockedUser.userToBlockId == request.Invited.userId));
    
                activityResult.Participants.forEach(request => {
                    if(request && request.Invited &&  request.Invited.Avatar){
                        request.Invited.avatarURL = "storage/"+request.Invited.Avatar.container+"/download/" + request.Invited.Avatar.name;
                    }
                });
    
    
                console.log("activity  ==== ",activityResult);


                callback(null,activityResult);
            }


        });
    }

    Activity.addPictures = function(options,activityId,ctx,callback){
        
        Activity.findOne({
            where : {
                activityId : activityId,
            }
        },function(err,activityResult){
            if(err){
                callback(err);
                return;
            }

            if(activityResult == null){
                callbackWithError("NotExists",404,callback);
                return;
            }

            const asset = Activity.app.models.Asset;
            ctx.req.params.container = 'activity';
    
            asset.uploadMultiple(ctx, null, function (err, uploadResult, fileObject) {
                if (err) {
                    callback(err);
                    asset.removeAssets(uploadResult, function (err, res) {});
                    return;
                }

                const ActivityPicture = Activity.app.models.ActivityPicture;
                let pictures = [];
                
                uploadResult.forEach(result => {
                    pictures.push({
                        activityId,
                        assetId:result.assetId,
                        addedBy : options.accessToken.userId
                    });
                });

                console.log("pictures === ",fileObject);

                ActivityPicture.create(pictures,function(err,result){
                    if(err){
                        callback(err);
                        return;
                    }

                    Activity.activityById(null,activityId,function(err,activityResult){
                        if(err){
                            callback(null);
                            return;
                        }
    
                        callback(null,activityResult);
    
                    });
                });
            });
        })
    }

    Activity.removePicture = function(options,activityId,pictureId,callback){

        Activity.findOne({
            where : {
                activityId : activityId,
            }
        },function(err,activityResult){
            if(err){
                callback(err);
                return;
            }

            if(activityResult == null){
                callbackWithError("NotExists",404,callback);
                return;
            }

            const ActivityPicture = Activity.app.models.ActivityPicture;
            ActivityPicture.findOne({
                where : {
                    and : [
                        {activityId : activityId},
                        {assetId : pictureId},
                    ]

                },
                include : {
                    relation : 'Picture'
                }
            },function(err,activityPictureResult){
                if(err){
                    callback(err);
                    return;
                }

                if(activityPictureResult == null){
                    callbackWithError("NotExists",404,callback);
                    return;
                }

                activityPictureResult = JSON.parse(JSON.stringify(activityPictureResult));

                console.log("activityPictureResult === ",activityPictureResult);

                const asset = Activity.app.models.Asset;

                asset.removeAsset(activityPictureResult.Picture.container,activityPictureResult.Picture.name, function (err, res) {
                    if(err){
                        callback(err);
                        return;
                    }

                    callback(null);
                });
            });
        });
    }

    Activity.comment = function(options,params,callback){

        Activity.activityById(null,params.activityId,function(err){
            if(err){
                callback(err);
                return;
            }

            const ActivityComment = Activity.app.models.ActivityComment;
            params.userId = options.accessToken.userId;

            ActivityComment.create(params,function(err){
                if(err){
                    callback(err);
                    return;
                }

                Activity.activityById(null,params.activityId,function(err,activityResult){
                    if(err){
                        callback(err);
                        return;
                    }

                    callback(null,activityResult)
                });
            });
        });
    }

    Activity.removeComment = function(options,commentId,callback){

        const ActivityComment = Activity.app.models.ActivityComment;

        ActivityComment.findOne({
            where : {
                activityCommentId : commentId,
                userId : options.accessToken.userId
            }
        },function(err,commentResult){
            if(err){
                callback(err);
                return;
            }

            if(commentResult == null){
                callbackWithError("NotExists",404,callback);
                return;
            }

            ActivityComment.destroyById(commentId,function(err){
                if(err){
                    callback(err);
                    return;
                }

                callback(null);
            });
        });
    }
}