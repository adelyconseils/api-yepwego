'use strict';

module.exports = function (InvitationRequest) {

    function callbackWithError(errorDescription, errorCode, callback) {
        var error = new Error(errorDescription);
        error.status = errorCode;
        callback(error);
    }

    InvitationRequest.invite = function(options, activityId, followers, callback){

        const Activity  = InvitationRequest.app.models.Activity;

        Activity.activityById(null,activityId,function(err,activityResult){
            if(err){
                callback(err);
                return;
            }

            InvitationRequest.destroyAll({
                senderUserId : options.accessToken.userId,
                invitedUserId : {inq : followers},
                activityId : activityId,
                invitationStateId : 1,
            },function(err,removeResult){
                if(err){
                    callback(err);
                    return;
                }

                let invitations = [];

                followers.forEach(followerId => {
                    invitations.push({
                        senderUserId : options.accessToken.userId,
                        invitedUserId : followerId,
                        activityId : activityId,
                        invitationStateId : 1,
                    });
                });
    
    
                InvitationRequest.create(invitations,function(err,result){
                    if(err){
                        callback(err);
                        return;
                    }
    
                    const Notification = InvitationRequest.app.models.Notification;
                    const Profile = InvitationRequest.app.models.Profile;
    
                    Profile.findOne({
                        where : {
                            userId : options.accessToken.userId
                        }
                    },function(err,profileResult){
                        if(!err){
                            const User = InvitationRequest.app.models.user;
    
                            User.find({
                                where : {
                                    id : {inq : followers}
                                }
                            },function(err,userResult){
    
                                console.log("userResult ==== ",userResult);
    
                                if(!err){
                                    Notification.sendNotification(
                                        userResult,
                                        null,
                                        (profileResult.pseudo || profileResult.firstName)+" t'a invité à participer à une activité de "+activityResult.Category.name,
                                        {activityId,invitationStateId : 1},
                                        function(err,result){});
                                }
                            });
                        }
                    });
    
    
    
                    callback(null);
                });
            });

            
        });
    }

    InvitationRequest.myInvitations = function(options,callback){

        InvitationRequest.find({
            where : {
                invitedUserId : options.accessToken.userId,
                invitationStateId : 1
            },
            order : "invitationRequestId DESC",
            include : [{
                relation : "Sender",
                scope : {
                    include : {
                        relation : "Avatar"
                    }
                }
            },
            {
                relation : "Activity",
                scope : {
                    include : {
                        relation : "Category"
                    }
                }
            }]
        },function(err,invitations){
            if(err){
                callback(err);
                return;
            }

            invitations = JSON.parse(JSON.stringify(invitations));

            invitations.forEach(request => {
                if(request && request.Sender &&  request.Sender.Avatar){
                    request.Sender.avatarURL = "storage/"+request.Sender.Avatar.container+"/download/" + request.Sender.Avatar.name;
                }
            });

            callback(null,invitations);

        })
    }

    InvitationRequest.myAcceptedInvitations = function(options,callback){

        InvitationRequest.find({
            where : {
                and : [
                    {or : [
                        {and : [{senderUserId : options.accessToken.userId},{isInvitation : true}]},
                        {and:[{invitedUserId : options.accessToken.userId},{isInvitation : false}]},
                    ]},
                    {invitationStateId : 2}
                ]
            },
            order : "invitationRequestId DESC",
            include : [
                {
                    relation : "Sender",
                    scope : {
                        include : {
                            relation : "Avatar"
                        }
                    }
                },
                {
                    relation : "Invited",
                    scope : {
                        include : {
                            relation : "Avatar"
                        }
                    }
                },
                {
                    relation : "Activity",
                    scope : {
                        include : {
                            relation : "Category"
                        }
                    }
                }
        ],
        },function(err,invitations){
            if(err){
                callback(err);
                return;
            }

            invitations = JSON.parse(JSON.stringify(invitations));

            invitations.forEach(request => {
                if(request && request.Sender &&  request.Sender.Avatar){
                    request.Sender.avatarURL = "storage/"+request.Sender.Avatar.container+"/download/" + request.Sender.Avatar.name;
                }

                if(request && request.Invited &&  request.Invited.Avatar){
                    request.Invited.avatarURL = "storage/"+request.Invited.Avatar.container+"/download/" + request.Invited.Avatar.name;
                }

            });

            callback(null,invitations);

        })
    }

    InvitationRequest.myPendingInvitations = function(options,callback){

        InvitationRequest.find({
            where : {
                senderUserId : options.accessToken.userId,
                invitationStateId : 5,
            },
            order : "invitationRequestId DESC",
            include : [
                {
                    relation : "Sender",
                    scope : {
                        include : {
                            relation : "Avatar"
                        }
                    }
                },
                {
                    relation : "Invited",
                    scope : {
                        include : {
                            relation : "Avatar"
                        }
                    }
                },
                {
                    relation : "Activity",
                    scope : {
                        include : {
                            relation : "Category"
                        }
                    }
                }
        ],
        },function(err,invitations){
            if(err){
                callback(err);
                return;
            }

            invitations = JSON.parse(JSON.stringify(invitations));

            invitations.forEach(request => {
                if(request && request.Sender &&  request.Sender.Avatar){
                    request.Sender.avatarURL = "storage/"+request.Sender.Avatar.container+"/download/" + request.Sender.Avatar.name;
                }

                if(request && request.Invited &&  request.Invited.Avatar){
                    request.Invited.avatarURL = "storage/"+request.Invited.Avatar.container+"/download/" + request.Invited.Avatar.name;
                }

            });

            callback(null,invitations);

        })
    }

    InvitationRequest.mySharedActivities = function(options,callback){

        InvitationRequest.find({
            where : {
                invitedUserId : options.accessToken.userId,
                invitationStateId : 4
            },
            order : "invitationRequestId DESC",
            include : [{
                relation : "Sender",
                scope : {
                    include : {
                        relation : "Avatar"
                    }
                }
            },
            {
                relation : "Activity",
                scope : {
                    include : {
                        relation : "Category"
                    }
                }
            }]
        },function(err,invitations){
            if(err){
                callback(err);
                return;
            }

            invitations = JSON.parse(JSON.stringify(invitations));

            invitations.forEach(request => {
                if(request && request.Sender &&  request.Sender.Avatar){
                    request.Sender.avatarURL = "storage/"+request.Sender.Avatar.container+"/download/" + request.Sender.Avatar.name;
                }
            });

            callback(null,invitations);

        });
    }

    InvitationRequest.respondToInvitation = function(requestId,options,accepted,callback){

        InvitationRequest.findOne({
            where : {
                invitationRequestId : requestId,
                invitedUserId : options.accessToken.userId,
                invitationStateId : 1
            },
            include : [
                {
                    relation : "Activity",
                    scope : {
                        include : {
                            relation : "Category"
                        }
                    }
                }
            ]
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            const resultJSON = JSON.parse(JSON.stringify(result));

            if(!result){
                callbackWithError("NotExists",404,callback);
                return;
            }

            if(accepted){
                result.updateAttribute("invitationStateId",2,function(err,updateResult){
                    if(err){
                        callback(err);
                        return;
                    }

                    const Notification = InvitationRequest.app.models.Notification;
                    const Profile = InvitationRequest.app.models.Profile;
    
                    Profile.findOne({
                        where : {
                            userId : options.accessToken.userId
                        }
                    },function(err,profileResult){
                        if(!err){
                            const User = InvitationRequest.app.models.user;

                            User.findOne({
                                where : {
                                    id : result.senderUserId
                                }
                            },function(err,userResult){

                                console.log("userResult ==== ",userResult);

                                if(!err){
                                    Notification.sendNotification(
                                        [userResult]
                                        ,null,
                                        (profileResult.pseudo || profileResult.firstName)+" a accepté ton invitation de participation à une activité de "+resultJSON.Activity.Category.name ,
                                        {activityId : result.activityId},
                                        function(err,result){});
                                }
                            });
                        }
                    });

                    callback(null);
                });
            }else{
                InvitationRequest.destroyById(requestId,function(err){
                    if(err){
                        callback(err);
                        return;
                    }

                    callback(null);
                });
            }


        });
    }

    InvitationRequest.cancelMyParticipation = function(activityId, options,callback){
     
        InvitationRequest.findOne({
            where : {
                activityId,
                invitedUserId : options.accessToken.userId,
            },
            include : [
                {
                    relation : "Activity"
                },
                {
                    relation : "Invited"
                }
            ]
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            if(!result){
                callbackWithError("NotExists",404,callback);
                return;
            }

            // InvitationRequest.destroyAll({
            //     activityId,
            //     invitedUserId : options.accessToken.userId,
            // },function(err){
            //     if(err){
            //         callback(err);
            //         return;
            //     }

            //     callback(null);
            // });



            const invitationResult = JSON.parse(JSON.stringify(result));

            InvitationRequest.destroyAll({
                activityId,
                invitedUserId : options.accessToken.userId,
            },function(err){
                if(err){
                    callback(err);
                    console.log("error ==== ",err);                        
                    return;
                }

                //not accepted
                if(invitationResult.invitationStateId == 5){
                    callback(null);
                    return;
                }

                //accepted
                var moment = require('moment');
                var now = moment(); //todays date
                var end = moment(invitationResult.Activity.startAt); // another date
                var duration = moment.duration(end.diff(now));
                var hours = duration.asHours();

                console.log("startAt === ",invitationResult.Activity.startAt);
                console.log("hours === ",hours);

                if(hours > 48){

                    result.Invited.get().then(function (profile) {
                        profile.updateAttribute("tokensCount",invitationResult.Invited.tokensCount + 1,function(err,updateResult){
                            console.log("update result ==== ",updateResult);
                        });
                    });
                }

                callback(null);
                        
            });

        });
    }

    InvitationRequest.shareActivity = function(options, activityId, followers, callback){
        const Activity  = InvitationRequest.app.models.Activity;

        Activity.activityById(null,activityId,function(err,activityResult){
            if(err){
                callback(err);
                return;
            }

            InvitationRequest.destroyAll({
                senderUserId : options.accessToken.userId,
                invitedUserId : {inq : followers},
                activityId : activityId,
                invitationStateId : 4,
            },function(err,removeResult){
                if(err){
                    callback(err);
                    return;
                }

                let invitations = [];

                followers.forEach(followerId => {
                    invitations.push({
                        senderUserId : options.accessToken.userId,
                        invitedUserId : followerId,
                        activityId : activityId,
                        invitationStateId : 4,
                    });
                });
    
                InvitationRequest.create(invitations,function(err,result){
                    if(err){
                        callback(err);
                        return;
                    }
    
    
                    const Notification = InvitationRequest.app.models.Notification;
                    const Profile = InvitationRequest.app.models.Profile;
    
                    Profile.findOne({
                        where : {
                            userId : options.accessToken.userId
                        }
                    },function(err,profileResult){
                        if(!err){
                            const User = InvitationRequest.app.models.user;
    
                                User.find({
                                    where : {
                                        id : {inq : followers}
                                    }
                                },function(err,userResult){
    
                                    console.log("userResult ==== ",userResult);
    
                                    if(!err){
                                        Notification.sendNotification(
                                            userResult,
                                            null,
                                            (profileResult.pseudo || profileResult.firstName)+" vient de créer une nouvelle activité "+activityResult.Category.name,
                                            {activityId},
                                            function(err,result){});
                                    }
                                });
    
                        }
                    });
    
                    callback(null);
                });
            });

            
        });
    }

    InvitationRequest.requestParticipation = function(options,activityId,responses,callback){
        const Activity  = InvitationRequest.app.models.Activity;
        const Profile = InvitationRequest.app.models.Profile;


        Profile.findOne({
            where : {
                userId : options.accessToken.userId 
            }
        },function(err,profileResult){
            if(err){
                callback(err);
                return;
            }

            if(!profileResult){
                callbackWithError("NotExists",404,callback);
                return;
            }

            if(!profileResult.tokensCount){
                callbackWithError("Tu n'as pas assez de jetons",409,callback);
                return;
            }

            Activity.activityById(null,activityId,function(err,activityResult){
                if(err){
                    callback(err);
                    return;
                }
    
                InvitationRequest.destroyAll({
                    senderUserId : options.accessToken.userId,
                    invitedUserId : activityResult.userId,
                    activityId : activityId,
                    invitationStateId : 5
                },function(err,removeResult){
                    if(err){
                        callback(err);
                        return;
                    }
    
    
                    InvitationRequest.create({
                        senderUserId : options.accessToken.userId,
                        invitedUserId : activityResult.userId,
                        activityId : activityId,
                        invitationStateId : 5,
                        isInvitation : false,
                        response1 : responses.response1,
                        response2 : responses.response2,
                        response3 : responses.response3,
                        response4 : responses.response4,
                    },function(err,result){
                        if(err){
                            callback(err);
                            return;
                        }
        
                        const Notification = InvitationRequest.app.models.Notification;
                        const Profile = InvitationRequest.app.models.Profile;
        
                        Profile.findOne({
                            where : {
                                userId : options.accessToken.userId
                            }
                        },function(err,profileResult){
                            if(!err){
                                const User = InvitationRequest.app.models.user;
        
                                    User.findOne({
                                        where : {
                                            id : activityResult.userId
                                        }
                                    },function(err,userResult){
        
                                        console.log("userResult ==== ",userResult);
        
                                        if(!err){
                                            Notification.sendNotification(
                                                [userResult],
                                                null,
                                                (profileResult.pseudo || profileResult.firstName)+" souhaite participer à ton activité de "+activityResult.Category.name,
                                                {activityId,invitationStateId : 5},
                                                function(err,result){});
                                        }
                                    });
                            }
                        });
        
                        callback(null);
                    });
    
                })
        
            });

        });




    }

    InvitationRequest.cancelRequestToParticipate = function(activityId, options,callback){
     
        InvitationRequest.findOne({
            where : {
                activityId,
                senderUserId : options.accessToken.userId,
            }
        },function(err,result){
            if(err){
                console.log("error ==== ",err);
                callback(err);
                return;
            }

            if(!result){
                callbackWithError("NotExists",404,callback);
                return;
            }

            console.log("result ==== ",result);

            InvitationRequest.destroyAll({
                activityId,
                senderUserId : options.accessToken.userId,
            },function(err){
                if(err){
                    callback(err);
                    console.log("error ==== ",err);

                    return;
                }

                callback(null);
            });
        });
    }

    InvitationRequest.myReceivedRequestToParticipate = function(options,callback){

        InvitationRequest.find({
            where : {
                invitedUserId : options.accessToken.userId,
                invitationStateId : 5
            },
            order : "invitationRequestId DESC",
            include : [ 
                {
                    relation : "Sender",
                    scope : {
                        include : [
                            {relation : "Avatar"},
                            {
                                relation : "Categories",
                                scope : {
                                    include : {
                                        relation : "Level"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    relation : "Activity",
                    scope : {
                        include : {
                            relation : "Category"
                        }
                    }
                }
            ]
        },function(err,invitations){
            if(err){
                callback(err);
                return;
            }

            invitations = JSON.parse(JSON.stringify(invitations));


            invitations.forEach(request => {
                if(request && request.Sender &&  request.Sender.Avatar){
                    request.Sender.avatarURL = "storage/"+request.Sender.Avatar.container+"/download/" + request.Sender.Avatar.name;
                }

                console.log("invitations  ===== ",request.Sender);
                const selectedCategory = request.Sender && request.Sender.Categories && request.Sender.Categories.find(category => request.Activity && (category.categoryId == request.Activity.categoryId));
                const level = selectedCategory ? selectedCategory.Level : null ;

                console.log("level  ===== ",level);

                request.Level = level;

            });

            callback(null,invitations);

        });
    }

    InvitationRequest.respondToRequestToParticipate = function(requestId,options,accepted,callback){

        InvitationRequest.findOne({
            where : {
                invitationRequestId : requestId,
                invitedUserId : options.accessToken.userId,
                invitationStateId : 5
            },
            include : [
                {
                    relation : "Activity",
                    scope : {
                        include : {
                            relation : "Category"
                        }
                    }
                }
            ]
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            if(!result){
                callbackWithError("NotExists",404,callback);
                return;
            }

            const resultJSON = JSON.parse(JSON.stringify(result));

            if(accepted){

                const params = {
                    invitationStateId : 2,
                    invitedUserId : result.senderUserId,
                    senderUserId : options.accessToken.userId
                }
                result.updateAttributes(params,function(err,updateResult){
                    if(err){
                        callback(err);
                        return;
                    }

                    const Notification = InvitationRequest.app.models.Notification;
                    const Profile = InvitationRequest.app.models.Profile;
    

                    Profile.findOne({
                        where : {
                            userId : result.invitedUserId
                        }
                    },function(err,requestedProfileResult){
                        if(!err && requestedProfileResult){
                            requestedProfileResult.updateAttribute("tokensCount",requestedProfileResult.tokensCount - 1,function(err,updateResult){});
                        }
                    });



                    Profile.findOne({
                        where : {
                            userId : options.accessToken.userId
                        }
                    },function(err,profileResult){
                        if(!err){
                            const User = InvitationRequest.app.models.user;

                            User.findOne({
                                where : {
                                    id : result.invitedUserId
                                }
                            },function(err,userResult){

                                console.log("userResult ==== ",userResult);

                                if(!err){
                                    Notification.sendNotification(
                                        [userResult],
                                        null,
                                        (profileResult.pseudo || profileResult.firstName)+" a accepté ta demande de participation à une activité de "+resultJSON.Activity.Category.name,
                                        {activityId : result.activityId},
                                        function(err,result){});
                                }
                            });
                        }
                    });

                    callback(null);
                });
            }else{
                InvitationRequest.destroyById(requestId,function(err){
                    if(err){
                        callback(err);
                        return;
                    }

                    callback(null);
                });
            }


        });
    }
}