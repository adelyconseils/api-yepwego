'use strict';

module.exports = function (Notification) {

  function callbackWithError(errorDescription, errorCode, callback) {
    var error = new Error(errorDescription);
    error.status = errorCode;
    callback(error);
  }

  Notification.sendNotification = function(to,title,body,data,callback){

    console.log("send to === ",to);
    console.log("data to send  === ",data);

    var request = require('request');

    const headers = {
      'Authorization': process.env.FIREBASE_SERVER_KEY,
      'Content-Type': 'application/json'
    }



    to.forEach(user => {

      console.log("user ==== ",user);

      Notification.notSeenNotificationsCount({accessToken:{userId: user.id}},function(err,notSeenNotifsResult){
          if(!err){

            data.badge = notSeenNotifsResult.total;

            const firebaseData = {
              "registration_ids": [user.fcmToken],
              "data" : data,
              "notification": {
                "body": body,
                "title": title,
                "sound": "default",
                "badge":notSeenNotifsResult.total
              }
            }

            const options = {
              uri: 'https://fcm.googleapis.com/fcm/send',
              method: 'POST',
              headers: headers,
              json: firebaseData
            }
        
            request(options, function (err, res, body) {
              if (err) {
                  // callback(err);
                  console.log("err notif === ",err);
                  return;
              }
              console.log(body);
              
            });
          }
      });
    });

    callback(null);

  }


  Notification.allNotifications = function(options,seen,callback){

    function compare(invitation1, invitation2) {
      const date1 = (invitation2.updatedAt || invitation2.createdAt);
      const date2 = (invitation1.updatedAt || invitation1.createdAt);
  
      // console.log("date 1 === ",invitation2.updatedAt , invitation2.createdAt );
      // console.log("date 2 === ",invitation1.updatedAt, invitation1.createdAt);
  
      var moment = require('moment');
  
      if ( date2.toString() > date1.toString())
      return -1;
      if (date2.toString() < date1.toString())
        return 1;
      return 0;
  
    }
    
    const FollowRequest = Notification.app.models.FollowRequest;
    const InvitationRequest = Notification.app.models.InvitationRequest;
    const Profile = Notification.app.models.Profile;
    var moment = require('moment');

    Profile.findOne({
      where : {
        userId : options.accessToken.userId
      }
    },function(err,profileResult){
        if(err){
          callback(err);
          return;
        }


        FollowRequest.myReceivedRequests(options,function(err,FollowRequests){
          if(err){
              callback(err);
              return;
          }
  
          FollowRequest.myAcceptedRequests(options,function(err,acceptedFollowRequests){
            if(err){
                callback(err);
                return;
            }
  
          InvitationRequest.myInvitations(options,function(err,ActivityInvitations){
            if(err){
              callback(err);
              return;
            }
  
            InvitationRequest.myAcceptedInvitations(options,function(err,acceptedActivityInvitations){
              if(err){
                callback(err);
                return;
              }
  
            InvitationRequest.mySharedActivities(options,function(err,ActivitiesShared){
              if(err){
                callback(err);
                return;
              }
  
              InvitationRequest.myReceivedRequestToParticipate(options,function(err,ActivitiesRequestsToParticipate){
                if(err){
                  callback(err);
                  return;
                }
  
                let recents = JSON.parse(JSON.stringify(acceptedActivityInvitations.concat(ActivitiesShared).concat(acceptedFollowRequests))) ;
                
                recents = recents.sort(compare);
  
                const lastSeenDate = profileResult.lastNotificationsSeenDate ; 

                ActivitiesRequestsToParticipate.forEach(request => {
                  const requestDate = (request.updatedAt || request.createdAt);
                  request.new = moment(requestDate).isAfter(lastSeenDate);
                });

                ActivityInvitations.forEach(request => {
                  const requestDate = (request.updatedAt || request.createdAt);
                  request.new = moment(requestDate).isAfter(lastSeenDate);
                });

                FollowRequests.forEach(request => {
                  const requestDate = (request.updatedAt || request.createdAt);
                  request.new = moment(requestDate).isAfter(lastSeenDate);
                });

                recents.forEach(request => {
                  const requestDate = (request.updatedAt || request.createdAt);
                  request.new = moment(requestDate).isAfter(lastSeenDate);
                });


                if(seen){
                  const Profile = Notification.app.models.Profile;
                  Profile.findOne({
                    where : {
                      userId : options.accessToken.userId
                    }
                  },function(err,profileResult){
                    if(err){
                      callback(err);
                      return;
                    }
  
                    profileResult.updateAttribute("lastNotificationsSeenDate",new Date(),function(err){
                      if(err){
                        callback(err);
                        return;
                      }
                      const result = {
                        ActivitiesRequestsToParticipate,
                        ActivityInvitations ,
                        FollowRequests,
                        Recent : recents
                      } 
            
                      callback(null,result);
    
                    });
                  });
                  return;
                }
  
                const result = {
                  ActivitiesRequestsToParticipate,
                  ActivityInvitations ,
                  FollowRequests,
                  Recent : recents
                } 
      
                callback(null,result);
  
  
              });
            });
          });
        });
      });
      });
    });
  }


  Notification.notSeenNotificationsCount = function(options,callback){

    console.log("options === ",options);

    var moment = require('moment');

    const Profile = Notification.app.models.Profile;
    Profile.findOne({
      where : {
        userId : options.accessToken.userId
      }
    },function(err,profileResult){
        if(err){
          callback(err);
          return;
        }

        Notification.allNotifications(options,false,function(err,notificationsResult){
          if(err){
            callback(err);
            return;
          }
    
          let counter = 0;
          const lastSeenDate = profileResult.lastNotificationsSeenDate ; 

          console.log("lastSeenDate ==== ",lastSeenDate);

          if(lastSeenDate){
            counter += notificationsResult.ActivitiesRequestsToParticipate.filter(request => {
              const requestDate = (request.updatedAt || request.createdAt);

              return moment(requestDate).isAfter(lastSeenDate);
            }).length;
            counter += notificationsResult.ActivityInvitations.filter(request => {
              const requestDate = (request.updatedAt || request.createdAt);

              return moment(requestDate).isAfter(lastSeenDate);
            }).length;
            counter += notificationsResult.FollowRequests.filter(request => {
              const requestDate = (request.updatedAt || request.createdAt);

              return moment(requestDate).isAfter(lastSeenDate);
            }).length;
            counter += notificationsResult.Recent.filter(request => {
              const requestDate = (request.updatedAt || request.createdAt);

              return moment(requestDate).isAfter(lastSeenDate);
            }).length;
        }else{
            counter += notificationsResult.ActivitiesRequestsToParticipate.length;
            counter += notificationsResult.ActivityInvitations.length;
            counter += notificationsResult.FollowRequests.length;
            counter += notificationsResult.Recent.length;
          }

          console.log("counter ==== ",counter);

          const Conversation = Notification.app.models.Conversation;

          Conversation.myConversations(options,function(err,conversationsResult){
            if(err){
              callback(err);
              return;
            }

            let messagesCount = 0 ;

            conversationsResult.forEach(conversation => {
              messagesCount += conversation.notSeenMessages; 
            });

            console.log("messagesCount ==== ",messagesCount);


            callback(null,
              {
                notifications:counter,
                messages : messagesCount,
                total : (messagesCount + counter)
              });


          });

    
        });
    });
  }


}