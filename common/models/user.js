'use strict';

var config = require('../../server/config.json');
var path = require('path');

module.exports = function (User) {
  User.disableRemoteMethodByName("create");
  User.disableRemoteMethodByName("upsert"); // disables PATCH /MyUsers
  User.disableRemoteMethodByName("find"); // disables GET /MyUsers
  User.disableRemoteMethodByName("replaceOrCreate"); // disables PUT /MyUsers
  User.disableRemoteMethodByName("prototype.updateAttributes"); // disables PATCH /MyUsers/{id}
  User.disableRemoteMethodByName("findById"); // disables GET /MyUsers/{id}
  User.disableRemoteMethodByName("exists"); // disables HEAD /MyUsers/{id}
  User.disableRemoteMethodByName("replaceById"); // disables PUT /MyUsers/{id2
  User.disableRemoteMethodByName("deleteById"); // disables DELETE /MyUsers/{id}
  User.disableRemoteMethodByName('prototype.__get__accessTokens'); // disable GET /MyUsers/{id}/accessTokens
  User.disableRemoteMethodByName('prototype.__create__accessTokens'); // disable POST /MyUsers/{id}/accessTokens
  User.disableRemoteMethodByName('prototype.__delete__accessTokens'); // disable DELETE /MyUsers/{id}/accessTokens
  User.disableRemoteMethodByName('prototype.__findById__accessTokens'); // disable GET /MyUsers/{id}/accessTokens/{fk}
  User.disableRemoteMethodByName('prototype.__updateById__accessTokens'); // disable PUT /MyUsers/{id}/accessTokens/{fk}
  User.disableRemoteMethodByName('prototype.__destroyById__accessTokens'); // disable DELETE /MyUsers/{id}/accessTokens/{fk}
  User.disableRemoteMethodByName('prototype.__count__accessTokens'); // disable  GET /MyUsers/{id}/accessTokens/count
  User.disableRemoteMethodByName("prototype.verify"); // disable POST /MyUsers/{id}/verify
  User.disableRemoteMethodByName("changePassword"); // disable POST /MyUsers/change-password
  User.disableRemoteMethodByName("createChangeStream"); // disable GET and POST /MyUsers/change-stream
  User.disableRemoteMethodByName("confirm"); // disables GET /MyUsers/confirm
  User.disableRemoteMethodByName("count"); // disables GET /MyUsers/count
  User.disableRemoteMethodByName("findOne"); // disables GET /MyUsers/findOne
  // User.disableRemoteMethodByName("login"); // disables POST /MyUsers/login
  //MyUser.disableRemoteMethodByName("logout");                               // disables POST /MyUsers/logout
  User.disableRemoteMethodByName("hasOne");
  // User.disableRemoteMethodByName("resetPassword"); // disables POST /MyUsers/reset
  // User.disableRemoteMethodByName("setPassword"); // disables POST /MyUsers/reset-password
  User.disableRemoteMethodByName("update"); // disables POST /MyUsers/update
  User.disableRemoteMethodByName("upsertWithWhere"); // disables POST /MyUsers/upsertWithWhere

  User.disableRemoteMethodByName('prototype.__get__Society'); 
  User.disableRemoteMethodByName('prototype.__create__Society'); 
  User.disableRemoteMethodByName('prototype.__delete__Society'); 
  User.disableRemoteMethodByName('prototype.__findById__Society'); 
  User.disableRemoteMethodByName('prototype.__update__Society'); 
  User.disableRemoteMethodByName('prototype.__destroy__Society'); 
  User.disableRemoteMethodByName('prototype.__count__Society');


  var Ajv = require('ajv');

  function generateCode() {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 8; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  User.validatesLengthOf('password', {
    min: 6,
    message: {
      min: 'Mot de passe doit contenir au moins 6 caractères'
    }
  });

  User.validatesUniquenessOf('username', {
    message: "Identifiant existe déjà"
  });

  User.validatesUniquenessOf('email', {
    message: "Email existe déjà"
  });

  User.on('resetPasswordRequest', function (info) {

    console.log("infos  ==== " + JSON.stringify(info));

    let url = process.env.BACKOFFICE_URL+'reset-password/'+ info.accessToken.id;


    var html = "<div style='margin: 3%; padding: 2%; text-align: center; border: 1px solid transparent; border-radius: 10px; background-color: #f3f3f3;'>" +
      "<p style = 'color:black;'>Quelqu'un (Espérons toi) a demandé une réinitialisation du mot de passe pour ton compte Yepwego.</p>" +
      "<p style = 'color:black;' >Si tu ne souhaites pas réinitialiser ton mot de passe, ne tiens pas compte de cet e-mail et aucune action ne sera prise.</p>" +
      "<a style='text-decoration: none; padding : 8px; color:white; text-align: center; border-radius: 5px; background-color: #49a0da;' href='" + url + "'>Réinitialiser le mot de passe</a>" +
      "</div>";

    var mailjet = require('node-mailjet')
       .connect(process.env.MAILJET_API_KEY, process.env.MAILJET_SECRET_KEY);
    var request = mailjet
      .post("send")
      .request({
        "FromEmail": process.env.EMAIL_SENDER,
        "FromName": process.env.FROM_EMAIL_NAME,
        "Subject": "Réinitialiser ton mot de passe",
        "Html-part": html,
        "Recipients": [{
          "Email": info.user.email
        }]
      });
    request.then((result) => {
        console.log(result.body)
      })
      .catch((err) => {
        console.log(err)
      });
  });

  User.auth = function (params,withoutVerification,callback) {

    var ajv = new Ajv({
      $data: true
    });
    var schema = {
      "type": "object",
      "properties": {
        "email": {
          "type": "string"
        },
        "username": {
          "type": "string"
        },
        "password": {
          "type": "string"
        }
      },
      "oneOf": [
        { "required": ["email", "password"]},
        { "required": ["username", "password"] }
      ],
    };

    var validate = ajv.compile(schema);

    if (!validate(params)) {
      var error = new Error("Bad credentials format");
      error.status = 400;
      callback(error);
      return;
    }
    User.login(params, function (err, token) {
      if (err) {
        callback(err);
        return;
      }

      console.log("entered");
      User.findOne({
        where: {
          or:[{email: params.email,username:params.username}]
        },
        fields: ['id', 'username', 'email','emailVerified','provider'],
      }, function (err, userResult) {
        if (err) {
          callback(err);
          return;
        }
        if (userResult == null) {
          var error = new Error("login failed");
          error.status = 401;
          callback(error);
          return;
        }

        if(!withoutVerification ){
          if(!userResult.emailVerified && userResult.provider == null){
            var error = new Error("email not verified");
            error.status = 409;
            callback(error);
            return;
          }
        }

        const Role = User.app.models.Role;
        Role.getRoles({
          principalId: userResult.id
        }, function (err, roles) {
          console.log(roles);
          if (err) {
            callback(err);
            return;
          }
          Role.findOne({
              where: {
                id: roles[2]
              },
              fields: ['id', 'name', 'description'],
            },
            function (error, roleRes) {
              if (err) {
                callback(err);
                return;
              }
              console.log(roleRes);

              const Profile = User.app.models.Profile;

              const options = {
                accessToken : token
              }

              Profile.myprofile(options,true,function(err,profileResult){
                if(err){
                  callback(err);
                  return;
                }

                if(!withoutVerification || userResult.provider != null ){
                  User.setLastLoginDate(options,function(err,result){});
                }

                const userResultt = JSON.parse(JSON.stringify(userResult));

                let userObject = {
                  token: token,
                  user: userResultt,
                  role: roleRes,
                  Profile:profileResult
                }


                  callback(null, userObject);

              });
            });
        });
      });
    });
  }

  User.providerauth = function (params, callback) {

    const Profile = User.app.models.Profile;
    const role = User.app.models.Role;
    const RoleMapping = User.app.models.RoleMapping;

    console.log("params === ",params);
    User.findOne({
      where : {
        username : params.User.username
      }
    },function(err,result){
      if(err){
        callback(err);
        return;
      }

      let userResult = JSON.parse(JSON.stringify(result));


      if(userResult != null){
        
        result.Profile.get().then(function (profileResult) {
          
          if((profileResult && profileResult.providerAvatarURL != params.Profile.providerAvatarURL) && (params.User.provider == 1)){
            profileResult.updateAttribute("providerAvatarURL",params.Profile.providerAvatarURL,function(err,updateResult){
              if(err){
                console.log("update avatar url error == ",err);
              }
              console.log("update result == ",updateResult);
            });
          }

          let usercred = {
            password : params.User.username,
            username : params.User.username
          }
  
  
          User.auth(usercred,true,function(err,result){
            if(err){
              callback(err);
              return;
            }
  
            callback(null,result);
          });

        });
        return;
      }


      User.beginTransaction('READ COMMITTED', function (err, tx) {

        params.User.password = params.User.username;
        params.User.email =  params.User.username+(params.User.provider == 1 ? "@fb.com" : "@google.com");
        params.User.emailVerified = true;

        User.create(params.User, {
          transaction: tx
        }, function (err, userResult) {
          if (err) {
            callback(err);
            tx.rollback(function (error) {});
            return;
          }
    
          role.findOne({
              where: {
                name: "MEMBER"
              }
            },
            function (error, roleRes) {
              if (error) {
                console.log(error);
                callback(err);
                tx.rollback(function (error) {});
                return;
              }
              // MAP ROLE  
              roleRes.principals.create({
                principalType: RoleMapping.USER,
                principalId: userResult.id,
                roleId: roleRes.id
              }, {
                transaction: tx
              }, function (err, principal) {
                if (err) {
                  callback(err);
                  tx.rollback(function (error) {});
                  return;
                }
    
                params.Profile.userId = userResult.id;
                params.Profile.sponsorshipCode = generateCode();

                if(params.User.provider == 2) params.Profile.providerAvatarURL+="?sz=250";

                Profile.create(params.Profile, {
                  transaction: tx
                }, function (err, profileResult) {
                  if (err) {
                    callback(err);
                    tx.rollback(function (error) {});
                    return;
                  }
    
                  tx.commit(function (error) {
                    let usercred = {
                      password : params.User.username,
                      username : params.User.username
                    }
                                
                    User.auth(usercred,true,function(err,result){
                      if(err){
                          console.log(err);
                          callback(null);
                          return;
                      }
                      const Category = User.app.models.Category;
      
                      let options = {
                        accessToken : {
                          userId : result.token.userId
                        }
                      };
      
                      Category.allCategories (options,function(err,categoriesResult){
                          result.Categories = categoriesResult;
                          result.firstSignin = true;
                          callback(null, result);
                      });
      
                  });
                    
                  });
                  
                });
              });
            });
        });
      });



    });
  }

  User.setEmailVerified = function(options,callback){

    User.findOne({
      where : {
        id : options.accessToken.userId,
      }
    },function(err,result){
      if(err){
        callback(err);
        return;
      }

      if (result == null) {
        var error = new Error("bad request");
        error.status = 400;
        callback(error);
        return;
      }

      result.updateAttribute("emailVerified",true,function(err,result){
        if(err){
          callback(err);
          return;
        }

        callback(null);
      });
    });
  }


  User.setLastLoginDate = function(options,callback){

    User.findOne({
      where : {
        id : options.accessToken.userId,
      }
    },function(err,result){
      if(err){
        callback(err);
        return;
      }

      if (result == null) {
        var error = new Error("bad request");
        error.status = 400;
        callback(error);
        return;
      }

      result.updateAttribute("lastLogin",new Date(),function(err,result){
        if(err){
          callback(err);
          console.log("update error ==== ",err);
          return;
        }

        callback(null);
      });
    });
  }


  User.checkEmailVerified = function(options,callback){
    User.findOne({
      where : {
        id : options.accessToken.userId,
      }
    },function(err,result){
      if(err){
        callback(err);
        return;
      }

      if (result == null) {
        var error = new Error("bad request");
        error.status = 400;
        callback(error);
        return;
      }

      if(result.emailVerified == false){
        var error = new Error("Email not verified");
        error.status = 401;
        callback(error);
        return;
      }

      callback(null);
    });
  }

  User.saveFCMToken = function(options,fcm,callback){

    console.log("fcm ==== ",fcm);

    User.findOne({
      where : {
        id : options.accessToken.userId
      }
    },function(err,userResult){
      if(err){
        callback(err);
        return;
      }

      userResult.updateAttribute("fcmToken",fcm,function(err,updateResult){
        if(err){
          callback(err);
          return;
        }

        callback(null);
      });
    });
  }

  User.signout = function (options, callback) {

    console.log("options === ", options);

    if (options.accessToken == null) {
      var errorObj = new Error("accessToken is required to logout");
      errorObj.status = 401;
      callback(errorObj);
      return;
    }

    User.logout(options.accessToken.id, function (err) {
      if (err) {
        callback(err);
        return;
      }

      User.updateAll({id : options.accessToken.userId},{fcmToken : null},function(err,result){});

      callback(null);

    });
  }


  User.contactUs = function(options,email,text,callback){


    var html = "<div style='margin: 3%; padding: 2%; text-align: left; border: 1px solid transparent; border-radius: 10px; background-color: #f3f3f3;'>" +
      "<p style = 'color:black;'>"+text+"</p>" +
      "<p style = 'color:black;' >Email de la part de : "+email+"</p>" +
      "</div>";

    var mailjet = require('node-mailjet')
       .connect(process.env.MAILJET_API_KEY, process.env.MAILJET_SECRET_KEY);
    var request = mailjet
      .post("send")
      .request({
        "FromEmail": process.env.EMAIL_SENDER,
        "FromName": process.env.FROM_EMAIL_NAME,
        "Subject": "Aide ("+email+")",
        "Html-part": html,
        "Recipients": [{
          "Email": process.env.EMAIL_CONTACT,
        }]
      });
    request.then((result) => {
        console.log(result.body)
        callback(null);
      })
      .catch((err) => {
        console.log(err)
        callback(err);
      });
  

  }

}