'use strict';

module.exports = function (Message) {

    function callbackWithError(errorDescription, errorCode, callback) {
        var error = new Error(errorDescription);
        error.status = errorCode;
        callback(error);
    }

    Message.new = function(options,params,callback){

        const Conversation = Message.app.models.Conversation ;

        params.sender = options.accessToken.userId;

        Conversation.conversationBy(options,null,params.participant,function(err,conversationResult){
            if(err && err.status != 404){
                callback(err);
                return;
            }

            if(err && err.status == 404){
                Conversation.new(options,{participant: params.participant},function(err,conversationResult){
                    if(err){
                        callback(err);
                        return;
                    }

                    params.conversationId = conversationResult.conversationId;
                    newMessage(params);
                    
                });

                return;
            }

            params.conversationId = conversationResult.conversationId;
            newMessage(params);

            function newMessage(params){
                Message.create(params,function(err,messageResult){
                    if(err){
                        callback(err);
                        return;
                    }

                    Message.sendNotification(options,params.conversationId,messageResult.message,function(err,res){});

                    callback(null);
                });
            }
        })

    }

    Message.sendNotification = function(options,conversationId,message,callback){

        const Conversation = Message.app.models.Conversation ;

        Conversation.conversationBy(options,conversationId,null,function(err,conversationResult){
            if(err){
                callback(err);
                return;
            }

            const Profile = Message.app.models.Profile;
            const Notification = Message.app.models.Notification;

            Profile.findOne({
                where : {
                    userId : options.accessToken.userId
                }
            },function(err,profileResult){
                if(!err){
                    const User = Message.app.models.user;
                    User.findOne({
                        where : {
                            id : conversationResult.Other.userId
                        }
                    },function(err,userResult){

                        console.log("userResult ==== ",userResult);

                        if(!err){
                            Notification.sendNotification(
                                [userResult],
                                "Nouveau message",
                                (profileResult.pseudo || profileResult.firstName)+" : "+message,
                                {conversationId: conversationResult.conversationId},
                            function(err,result){
                            });
                        }
                    });

                }
            });
            
        });
    }
}