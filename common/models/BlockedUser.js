'use strict';

module.exports = function (BlockedUser) {

    BlockedUser.blockUser = function(options, userId, callback){

        const params = {
            userId : options.accessToken.userId,
            userToBlockId : userId
        }
        BlockedUser.beginTransaction('READ COMMITTED', function (err, tx) {
            BlockedUser.create(params,{transaction : tx},function(err,result){
                if(err){
                    tx.rollback(function (error) {
                        callback(err);
                    });
                    return;
                }
    
                const FollowRequest = BlockedUser.app.models.FollowRequest;
                FollowRequest.destroyAll({
                    or : [
                        {
                            and : [
                                {followerId : options.accessToken.userId},
                                {followedId : userId},
                            ]
                        },
                        {
                            and : [
                                {followedId : options.accessToken.userId},
                                {followerId : userId},
                            ]
                        }
                    ]
                },{transaction : tx},function(err,destroyResut){
                    if(err){
                        tx.rollback(function (error) {
                            callback(err);
                        });
                        return;
                    }
    
                    const InvitationRequest = BlockedUser.app.models.InvitationRequest ; 
                    InvitationRequest.destroyAll({
                        or : [
                            {
                                and : [
                                    {senderUserId : options.accessToken.userId},
                                    {invitedUserId : userId},
                                ]
                            },
                            {
                                and : [
                                    {invitedUserId : options.accessToken.userId},
                                    {senderUserId : userId},
                                ]
                            }
                        ]
                    },{transaction : tx},function(err,destroyResult){
                        if(err){
                            tx.rollback(function (error) {
                                callback(err);
                            });
                            return;
                        }

                        const Conversation = BlockedUser.app.models.Conversation;
                        Conversation.removeUserConversation(options,userId,tx,function(err,result){
                            if(err && err.status != 404){
                                tx.rollback(function (error) {
                                    callback(err);
                                });
                                return;
                            }

                            tx.commit(function (err) {
                                callback(null);
                            });
                        });
                    });
                });
            });
        });

    }

    BlockedUser.blockedUsers = function(options, callback){

        BlockedUser.find({
            where : {
                or : [
                    {userId : options.accessToken.userId},
                    {userToBlockId : options.accessToken.userId},
                ]
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            callback(null,result);
        })
    }
}