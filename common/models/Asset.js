'use strict'

module.exports = function (Asset) {

  function generateCode() {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 6; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }


  Asset.uploadMultiple = function (ctx, options, callback) {

    var StorageContainer = Asset.app.models.Storage;
    if (!options) options = {};
    StorageContainer.upload(ctx.req.params.container,ctx.req, ctx.result, {
      container: ctx.req.params.container,
      getFilename: function (fileInfo, req, res) {
        var origFilename = fileInfo.name;
        var parts = origFilename.split('.'),
          extension = parts[parts.length - 1];
        var newFilename = (new Date()).getTime() + generateCode() + '.' + extension;
        console.log("filename ==== ",newFilename);
        return newFilename;
      }
    }, function (err, fileObj) {
      if (err) {
        console.log("upload error",fileObj);
        callback(err);
      } else {
        console.log(fileObj.files);
        let assetObjects = fileObj.files;
        var assetParams = [];

        if (assetObjects["avatar"] && assetObjects["avatar"].length > 0) {

          let attachements = assetObjects["avatar"];
          attachements.forEach(attachement => {
            assetParams.push({
              name: attachement.name,
              type: attachement.type,
              container: attachement.container,
              for: "profile"
            });
          });
          
        }

        if (assetObjects["cover"] && assetObjects["cover"].length > 0) {

          let attachements = assetObjects["cover"];
          attachements.forEach(attachement => {
            assetParams.push({
              name: attachement.name,
              type: attachement.type,
              container: attachement.container,
              for: "profile"
            });
          });
          
        }

        if (assetObjects["categoryCover"] && assetObjects["categoryCover"].length > 0) {

          let attachements = assetObjects["categoryCover"];
          attachements.forEach(attachement => {
            assetParams.push({
              name: attachement.name,
              type: attachement.type,
              container: attachement.container,
              for: "category"
            });
          });
          
        }

        if (assetObjects["activityPicture"] && assetObjects["activityPicture"].length > 0) {

          let attachements = assetObjects["activityPicture"];
          attachements.forEach(attachement => {
            assetParams.push({
              name: attachement.name,
              type: attachement.type,
              container: attachement.container,
              for: "activity"
            });
          });
          
        }

        // console.log("assetparams === " + assetParams);
        Asset.create(assetParams, function (err, assetResult) {
          if (err) {
            console.log("create asset error");
            callback(err);
          } else {
            console.log(assetResult);
            callback(null, assetResult, fileObj);
          }
        });
      }
    });
  }

  function callbackWithError(errorDescription, errorCode, callback) {
    var error = new Error(errorDescription);
    error.status = errorCode;
    callback(error);
}

  Asset.removeAsset = function (container, assetName, callback) {
    console.log("constainer == ",container,assetName);
    var StorageContainer = Asset.app.models.Storage;
    if (container != null && assetName != null) {
      StorageContainer.removeFile(container, assetName, function (err, result) {
        console.log(result);
        console.log("asset name ==== " + assetName);
        Asset.findOne({
          where: {
            name: assetName
          }
        }, function (err, result) {
          if (result != null) {
            Asset.destroyById(result.assetId, function (err, res) {
              console.log(res);
              callback(null);
            });
          }
        })

      });
    }else{
      callbackWithError("Image n'existe pas",404,callback);
    }
  }

  Asset.removeAssets = function (assets, callback) {
    console.log('removeAssets called',assets);
    if (assets && assets.length > 0) {
      assets.forEach(assetObj => {
        Asset.removeAsset(assetObj.container, assetObj.name, function (err, result) {
          if (err) {
            callback(err);
            console.log("remove asset error = ", err);     

            return;
          }
          console.log("remove asset result = ", result);
        });
      });

    }
  }
}