'use strict';

module.exports = function (Conversation) {

    var moment = require('moment');

    function callbackWithError(errorDescription, errorCode, callback) {
        var error = new Error(errorDescription);
        error.status = errorCode;
        callback(error);
    }

    Conversation.new = function(options,params,callback){

        params.creator = options.accessToken.userId;

        Conversation.create(params,function(err,conversationResult){
            if(err){
                callback(err);
                return;
            }

            callback(null,conversationResult);
        });
    }


    Conversation.conversationBy = function(options,conversationId,participantId,callback){
        
        let whereFilter ;

        if(conversationId){
            whereFilter = {
                and : [
                    {conversationId},
                    {or : [
                        {creator : options.accessToken.userId},
                        {participant : options.accessToken.userId}
                    ]}
                ]
            }
        }else{
            whereFilter = {
                or : [
                    {and : [
                        {creator : options.accessToken.userId},
                        {participant : participantId}
                    ]},
                    {and : [
                        {participant : options.accessToken.userId},
                        {creator : participantId}
                    ]}
                ]
            }
        }

        Conversation.findOne({
            where : whereFilter,
            include : [
                {
                    relation : "Messages",
                    scope : {
                        order : "messageId DESC",
                        include : {
                            relation : "Sender",
                            scope : {
                                fields : ["firstName","pseudo","avatarId","providerAvatarURL"],
                                include : {
                                    relation : "Avatar",
                                }
                            }
                        }
                    }
                },
                {
                    relation : "Creator",
                    scope : {
                        fields : ["firstName","pseudo","avatarId","providerAvatarURL"],
                        include : {
                            relation : "Avatar",
                        }
                    }
                },
                {
                    relation : "Participant",
                    scope : {
                        fields : ["firstName","pseudo","avatarId","providerAvatarURL"],
                        include : {
                            relation : "Avatar",
                        }
                    }
                }
            ]
        },function(err,conversationResult){
            if(err){
                callback(err);
                return;
            }

            if(!conversationResult){
                callbackWithError("NotExists",404,callback);
                return;
            }

            let conversationResultCopy = JSON.parse(JSON.stringify(conversationResult));

            let other = conversationResultCopy.Creator.userId != options.accessToken.userId ? conversationResultCopy.Creator : conversationResultCopy.Participant;

            if(other && other.Avatar){
                other.avatarURL = "storage/"+other.Avatar.container+"/download/" + other.Avatar.name;
            }

            conversationResultCopy.Other = other;

            delete conversationResultCopy.Creator;
            delete conversationResultCopy.Participant;

            let customMesages = [];

            const lastSeenDate = conversationResultCopy.creator == options.accessToken.userId ? conversationResultCopy.creatorLastSeenMessages : conversationResultCopy.participantLastSeenMessages ; 
            let notSeenMessages = 0;


            conversationResultCopy.Messages.forEach(message => {
                
                
                if(message.Sender && message.Sender.Avatar){
                    message.Sender.avatarURL = "storage/"+message.Sender.Avatar.container+"/download/" + message.Sender.Avatar.name;
                }

                if(!lastSeenDate || (lastSeenDate && moment(message.createdAt).isAfter(lastSeenDate))){
                    notSeenMessages++ ;
                } 


                customMesages.push({
                    _id: message.messageId,
                    text : message.message,
                    createdAt : message.createdAt,
                    user : {
                        _id : message.sender == options.accessToken.userId ? 1 : 2,
                        name : message.Sender.pseudo || message.Sender.firstName,
                        avatar : message.Sender.avatarURL ? process.env.API_URL+message.Sender.avatarURL : message.Sender.providerAvatarURL 
                    } 
                });

            });

            conversationResultCopy.notSeenMessages = notSeenMessages;
            conversationResultCopy.Messages = customMesages;

            conversationResult.updateAttribute(conversationResultCopy.creator == options.accessToken.userId ? "creatorLastSeenMessages" : "participantLastSeenMessages",new Date(),function(err){});

            callback(null,conversationResultCopy);
        });
    }


    Conversation.myConversations = function(options,callback){
        
        Conversation.find({
            where : {
                or : [
                    {creator : options.accessToken.userId},
                    {participant : options.accessToken.userId}
                ]
            },
            include : [
                {
                    relation : "LastMessage",
                },
                {
                    relation : "Creator",
                    scope : {
                        fields : ["firstName","pseudo","avatarId","providerAvatarURL"],
                        include : {
                            relation : "Avatar",
                        }
                    }
                },
                {
                    relation : "Participant",
                    scope : {
                        fields : ["firstName","pseudo","avatarId","providerAvatarURL"],
                        include : {
                            relation : "Avatar",
                        }
                    }
                },
                {
                    relation : "Messages",
                }
            ]
        },function(err,conversationResult){
            if(err){
                callback(err);
                return;
            }

            conversationResult = JSON.parse(JSON.stringify(conversationResult));

            conversationResult = conversationResult.filter(conv => conv.LastMessage);

            conversationResult.forEach(conversation => {

                let other = conversation.Creator.userId != options.accessToken.userId ? conversation.Creator : conversation.Participant;

                if(other && other.Avatar){
                    other.avatarURL = "storage/"+other.Avatar.container+"/download/" + other.Avatar.name;
                }

                conversation.Other = other;

                const lastSeenDate = conversation.creator == options.accessToken.userId ? conversation.creatorLastSeenMessages : conversation.participantLastSeenMessages ; 
                conversation.notSeenMessages = conversation.Messages.filter(message => {
                    if(message.sender != options.accessToken.userId){
                        return !lastSeenDate || (lastSeenDate && moment(message.createdAt).isAfter(lastSeenDate))
                    }
                }).length;

                delete conversation.Creator;
                delete conversation.Participant;
                delete conversation.Messages;
            });

            conversationResult.sort(function(convA,convB){
                if ( convA.LastMessage.messageId > convB.LastMessage.messageId)
                return -1;
                if (convA.LastMessage.messageId < convB.LastMessage.messageId)
                  return 1;
                return 0;
            })


            callback(null,conversationResult);
        });
    }


    Conversation.removeConversation = function(options,conversationId,callback){

        Conversation.conversationBy(options,conversationId,null,function(err,conversationResult){
            if(err){
                callback(err);
                return;
            }

            Conversation.destroyById(conversationId,function(err){
                if(err){
                    callback(err);
                    return;
                }

                callback(null);
            });
        });
    }

    Conversation.removeUserConversation = function(options,participantId,tx,callback){

        Conversation.conversationBy(options,null,participantId,function(err,conversationResult){
            if(err){
                callback(err);
                return;
            }

            Conversation.destroyById(conversationResult.conversationId,{transaction : tx},function(err){
                if(err){
                    callback(err);
                    return;
                }

                callback(null);
            });
        });
    }

}