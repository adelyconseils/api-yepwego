'use strict';

module.exports = function (PaymentMethod) {
    
    const Ajv = require('ajv');

    function callbackWithError(errorDescription, errorCode, callback) {
        var error = new Error(errorDescription);
        error.status = errorCode;
        callback(error);
    }

    PaymentMethod.new = function(options,params,callback){

        var ajv = new Ajv({
            $data: true
        });
        var schema = {
            "type": "object",
            "properties": {
                "cardNumber": {
                    "type": "string"
                },
                "expiryMonth": {
                    "type": "number"
                },
                "expiryYear": {
                    "type": "number"
                }
            },
            "additionalProperties": false,
            "required": ["expiryYear","expiryMonth","cardNumber"] 
        };

        var validate = ajv.compile(schema);

        if (!validate(params)) {
            var error = new Error("Format invalide");
            error.status = 400;
            callback(error);
            return;
        }


        params.userId = options.accessToken.userId;

        PaymentMethod.create(params,function(err,createResult){
            if(err){
                callback(err);
                return;
            }

            callback(null);
        });

    }

    PaymentMethod.removePaymentMethod = function(options,id,callback){

        PaymentMethod.findOne({
            where : {
                paymentMethodId : id,
                userId : options.accessToken.userId
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            if(!result){
                callbackWithError("NotExists",404,callback);
                return;
            }

            PaymentMethod.destroyById(id,function(err){
                if(err){
                    callback(err);
                    return;
                }

                callback(null);

            });
        });
    }



}