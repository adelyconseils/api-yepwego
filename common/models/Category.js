'use strict';

module.exports = function (Category) {

    const isJSON = require('is-json');
    const Ajv = require('ajv');

    function removeAsset(container, assetName) {

        var StorageContainer = Category.app.models.Storage;
        var asset = Category.app.models.Asset;
    
        console.log("container== ", container);
        console.log("assetName== ", assetName);
    
        if (container != null && assetName != null) {
          console.log("entered");
          StorageContainer.removeFile(container, assetName, function (err, result) {
            console.log(result);
            console.log("asset name ==== " + assetName);
            asset.findOne({
              where: {
                name: assetName
              }
            }, function (err, result) {
              if (result != null) {
                asset.destroyById(result.assetId, function (err, res) {
                  console.log(res);
                });
              }
            });
          });
        }
    }
    
    function callbackWithError(errorDescription, errorCode, callback) {
        var error = new Error(errorDescription);
        error.status = errorCode;
        callback(error);
    }
    
    function removeAssett(uploadResult) {
        if (uploadResult != null) {
    
          var StorageContainer = Category.app.models.Storage;
          var asset = Category.app.models.Asset;
    
          let container = uploadResult.container;
          let assetName = uploadResult.name;
    
          if (container != null && assetName != null) {
    
            StorageContainer.removeFile(container, assetName, function (err, result) {
              console.log(result);
              console.log("asset name ==== " + assetName);
              asset.findOne({
                where: {
                  name: assetName
                }
              }, function (err, result) {
                if (result != null) {
                  asset.destroyById(result.assetId, function (err, res) {
                    console.log(res);
                  });
                }
              })
    
            });
          }
        }
    
    }

    function distance(lat1, lon1, lat2, lon2, unit) {
      if ((lat1 == lat2) && (lon1 == lon2)) {
          return 0;
      }
      else {
          var radlat1 = Math.PI * lat1/180;
          var radlat2 = Math.PI * lat2/180;
          var theta = lon1-lon2;
          var radtheta = Math.PI * theta/180;
          var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
          if (dist > 1) {
              dist = 1;
          }
          dist = Math.acos(dist);
          dist = dist * 180/Math.PI;
          dist = dist * 60 * 1.1515;
          if (unit=="K") { dist = dist * 1.609344 }
          if (unit=="N") { dist = dist * 0.8684 }
          return dist;
      }
    }

    Category.new = function(ctx, callback){
        const asset = Category.app.models.Asset;
        ctx.req.params.container = 'category';
    
        asset.uploadMultiple(ctx, null, function (err, uploadResult, fileObject) {
          if (err) {
            callback(err);
            asset.removeAssets(uploadResult, function (err, res) {});
            return;
          }
    
    
          Category.beginTransaction('READ COMMITTED', function (err, tx) {
    
            console.log("[transaction ", tx.id, "] started");
    
            Category.saveCategoryInfos(uploadResult, fileObject, tx, function (err, categoryResult) {
              if (err) {
                tx.rollback(function (error) {
                  asset.removeAssets(uploadResult, function (err, res) {});
                  console.log("[transaction ", tx.id, "] rolledback");
                  callback(err);
                });
                return;
              }
    
              tx.commit(function (error) {
                console.log("[transaction ", tx.id, "] commited");
                // Category.categoryById(categoryResult,function(err,result){
                //     if(err){
                //         console.log(err);
                //         callback(null);
                //         return;
                //     }
                //     callback(null, result);
                // });

                callback(null);
              });
            });
          });
        });
    }

    Category.saveCategoryInfos = function (uploadResult, fileObject, tx, callback) {

        console.log("uploadResult === ", uploadResult);
        console.log("fileObject === ", fileObject);
    
        const asset = Category.app.models.Asset;
    
        if (!fileObject.fields['data'] || !fileObject.fields['data'][0]) {
          callbackWithError("data requise", 400, callback);
          return;
        }
    
        let params = fileObject.fields['data'][0];
    
        if (!isJSON(params)) {
          callbackWithError("Format d'objet invalide", 400, callback);
          return;
        }
    
        params = JSON.parse(params);
    
        var ajv = new Ajv({
          $data: true
        });
    
        var schema = {
          "type": "object",
          "properties": {
            "name": {
              "type": "string",
            },
            "hasLevel": {
              "type": "boolean",
            }
          },
          "required": ['name'],
          "additionalProperties": false
        }
    
        var validate = ajv.compile(schema);
    
        if (!validate(params)) {
          callbackWithError("Format invalide", 400, callback);
          return;
        }

        if (uploadResult != null && uploadResult.length > 0 && uploadResult[0].for == "category") {
            params.coverId = uploadResult[0].assetId;
        }

        Category.create(params, {
            transaction: tx
        }, function (err, categoryResult) {
            if (err) {
              callback(err);
              return;
            }

            callback(null,categoryResult);
        });
    }

    Category.allCategories = function(options,callback){
        Category.find({
            include:[
                {
                    relation:'Cover'
                },
                {
                  relation:'Activities',
                  scope:{
                    include : [
                      {
                        relation : "Address"
                      }
                    ]
                  }
                }
            ]
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            result = JSON.parse(JSON.stringify(result));

            result.forEach(category => {
                if(category.Cover){
                    category.coverURL = "storage/"+category.Cover.container+"/download/" + category.Cover.name;
                }
                
            });

            if(options.accessToken){
                const SelectedCategory = Category.app.models.SelectedCategory;
              SelectedCategory.find({
                where : {
                  userId : options.accessToken.userId,
                }
              },function(err,SelectedCatResult){
                if(err){
                  callback(err);
                  return;
                }

                SelectedCatResult.forEach(selected => {
                  result.forEach(category => {
                      if(category.categoryId == selected.categoryId){
                        category.selected = true;
                        category.levelId = selected.levelId
                      }
                  });
                });

                callback(null,result);

              });
            }else{
              callback(null,result);
            }
        });
    }

    Category.setSelectedCategories = function(params,options,callback){

      const SelectedCategory = Category.app.models.SelectedCategory;

      Category.beginTransaction('READ COMMITTED', function (err, tx) {

        SelectedCategory.destroyAll({
          userId: options.accessToken.userId
        },{transaction : tx},function(err,result){
          if(err){
            callback(err);
            tx.rollback(function (error) {});
            return;
          }

          if(params.length){
            params.forEach(cat => {
              cat.userId = options.accessToken.userId
            })
  
            SelectedCategory.create(params,{transaction : tx},function(err,result){
              if(err){
                callback(err);
                tx.rollback(function (error) {});
                return;
              }
  
              tx.commit(function (error) {
                Category.allCategories(options,function(err,categoriesResult){
              if(err){
                callback(err);
                return;
              }
              callback(null,categoriesResult);
            });
              });
            });
          }else{
            tx.commit(function (error) {
              Category.allCategories(options,function(err,categoriesResult){
              if(err){
                callback(err);
                return;
              }
              callback(null,categoriesResult);
            });
            });
          }
        });
      });

    }

    Category.categoryById = function(options,categoryId,lat,long,fromDate,toDate,callback){

      var moment = require('moment');

      function compare(activity1,activity2) {

        const firstDistance = distance(lat,long,parseFloat(activity1.Address.lattitude) ,parseFloat(activity1.Address.longitude));
        const secondDistance = distance(lat,long,parseFloat(activity2.Address.lattitude),parseFloat(activity2.Address.longitude));

        if (firstDistance < secondDistance)
          return -1;
        if (firstDistance > secondDistance)
          return 1;
        return 0;
      }


      console.log("lat === ",lat);
      console.log("long === ",long);
      

      console.log("category id === ",categoryId);

        const SelectedCategory = Category.app.models.SelectedCategory;
        const BlockedUser = Category.app.models.BlockedUser;

        BlockedUser.blockedUsers(options,function(err,blockedUsersResult){
            if(err){
                callback(err);
                return;
            }

            SelectedCategory.findOne({
              where : {
                  userId : options.accessToken.userId,
                  categoryId : categoryId
              }
          },function(err,SelectedCatResult){
              if(err){
                  callback(err);
                  return;
              }
  
              SelectedCatResult = JSON.parse(JSON.stringify(SelectedCatResult));
  
              Category.findOne({
                where : {
                  categoryId : categoryId
                },
                include:[
                  {
                    relation : "Activities",
                    scope:{
                      include : [
                        {
                          relation : "Category",
                        },
                        {
                          relation : "Address",
                        },
                        {
                          relation:"Creator"
                        },
                        {
                          relation:"Comments"
                        },
                        {
                          relation : "Participants",
                          scope : {
                            where : {
                              invitationStateId : 2
                            }
                          }
                        }
                      ]
                    }
                  },{
                    relation : "Cover"
                  }
                ]
              },function(err,categoryResult){
                if(err){
                  callback(err);
                  return;
                }
        
                if(categoryResult == null){
                  callbackWithError("NotExists",404,callback);
                  return;
                }
        
                categoryResult = JSON.parse(JSON.stringify(categoryResult));
        
                if(categoryResult.Cover){
                  categoryResult.coverURL = "storage/"+categoryResult.Cover.container+"/download/" + categoryResult.Cover.name;
                }
        
                const now = moment().format("YYYY-MM-DD");
        
                if(fromDate || toDate){
        
                  const fromdate = moment(fromDate, "YYYY-MM-DD");
                  const todate = moment(toDate, "YYYY-MM-DD").add(1, 'days');
        
                  categoryResult.Activities = categoryResult.Activities.filter(activity => toDate ? moment(activity.startAt, "YYYY-MM-DD").isBetween(fromdate,todate,'days','[]')  : moment(activity.startAt,"YYYY-MM-DD").isSameOrAfter(fromdate))
        
                }else{
                  categoryResult.Activities = categoryResult.Activities.filter(activity =>  moment(activity.startAt,"YYYY-MM-DD").isSameOrAfter(now))
                }
        
                categoryResult.Activities = categoryResult.Activities.filter(activity => {
                  const userDist = distance(lat,long,parseFloat(activity.Address.lattitude) ,parseFloat(activity.Address.longitude));
                  return userDist <= 50;
                });
        
                categoryResult.Activities = categoryResult.Activities.filter(activity => 
                  (activity.isPublic && activity.userId != options.accessToken.userId) && 
                  (blockedUsersResult.find(BlockedRelation => (BlockedRelation.userId == options.accessToken.userId && BlockedRelation.userToBlockId ==  activity.userId ) || (BlockedRelation.userToBlockId == options.accessToken.userId && BlockedRelation.userId ==  activity.userId ))) == null);
        
                if(!fromDate && SelectedCatResult && SelectedCatResult.levelId && categoryResult.Activities.filter(activity => activity.searchedLevel <= SelectedCatResult.levelId).length){
                  categoryResult.Activities = categoryResult.Activities.filter(activity => activity.searchedLevel <= SelectedCatResult.levelId);
                }
        
                categoryResult.Activities.sort(compare);
        
                callback(null,categoryResult);
              });
  
          });

        });
    }
    
}