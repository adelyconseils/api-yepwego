'use strict';

module.exports = function (Profile) {

//   Profile.validateAsync('pseudo', validatePseudoUnique, {
//     message: 'Pseudo existe déjà'
//   });


//   function validatePseudoUnique(err, done) {
//     const pseudo = this.pseudo;
//     const userId = this.userId;


//     if (pseudo) {
//         let whereFilter = {
//             pseudo: pseudo
//         };

//         if (this.userId) {
//             whereFilter.userId = {
//                 neq: userId
//             }
//         }

//         process.nextTick(function () {
//             Profile.findOne({
//                 where: whereFilter
//             }, function (error, result) {
//                 if (error) {
//                     err();
//                 } else if (result != null) {
//                     err();
//                 }

//                 done();
//             })
//         });

//     } else {
//         process.nextTick(function () {
//             done();
//         });
//     }
// }

    function removeAsset(container, assetName) {

        var StorageContainer = Profile.app.models.Storage;
        var asset = Profile.app.models.Asset;
    
        console.log("container== ", container);
        console.log("assetName== ", assetName);
    
        if (container != null && assetName != null) {
          console.log("entered");
          StorageContainer.removeFile(container, assetName, function (err, result) {
            console.log(result);
            console.log("asset name ==== " + assetName);
            asset.findOne({
              where: {
                name: assetName
              }
            }, function (err, result) {
              if (result != null) {
                asset.destroyById(result.assetId, function (err, res) {
                  console.log(res);
                });
              }
            });
          });
        }
    }
    
    function callbackWithError(errorDescription, errorCode, callback) {
      var error = new Error(errorDescription);
      error.status = errorCode;
      callback(error);
    }
  
    function removeAssett(uploadResult) {
      if (uploadResult != null) {
  
        var StorageContainer = Profile.app.models.Storage;
        var asset = Profile.app.models.Asset;
  
        let container = uploadResult.container;
        let assetName = uploadResult.name;
  
        if (container != null && assetName != null) {
  
          StorageContainer.removeFile(container, assetName, function (err, result) {
            console.log(result);
            console.log("asset name ==== " + assetName);
            asset.findOne({
              where: {
                name: assetName
              }
            }, function (err, result) {
              if (result != null) {
                asset.destroyById(result.assetId, function (err, res) {
                  console.log(res);
                });
              }
            })
  
          });
        }
      }
  
    }

    Profile.memberProfile = function (options,memberId, callback) {


      function compare(activity1, activity2) {
        const date1 = (activity2.startAt);
        const date2 = (activity1.startAt);
    
    
        if ( date2.toString() > date1.toString())
        return 1;
        if (date2.toString() < date1.toString())
          return -1;
        return 0;
    
      }

        Profile.findOne({
            where:{
                userId : memberId
            },
            include : [
                {
                    relation : "Avatar",
                },
                {
                    relation : "Cover",
                },
                {
                    relation : "User",
                },
                {
                  relation : "PaymentMethods"
                },  
                {
                  relation : "FollowRequest",
                  scope : {
                    where : {
                      followerId : options.accessToken.userId
                    }
                  }
                },
                {
                  relation : "ReceivedFollowRequest",
                  scope : {
                    where : {
                      followedId : options.accessToken.userId
                    }
                  }
                },
                {
                  relation : "Followers",
                  scope : {
                    where : {
                      requestStateId : 2
                    }
                  }
                },
                {
                  relation : "Activities",
                  scope : {
                    where : {
                      isPublic : options.accessToken.userId == memberId ? {inq : [true,false]} : true,
                    },
                    include:[
                      {
                          relation:'Category',
                          scope:{
                              include : {
                                  relation : "Cover"
                              }
                          }
                      },
                      {
                        relation : "Participants",
                        scope : {
                          where : {
                            invitationStateId : 2
                          }
                        }
                      },
                      {
                          relation : "Address",
                      },
                      {
                          relation:"Creator",
                          scope : {
                              include : [
                                  {
                                      relation : "Avatar"
                                  }
                              ]
                          }
                      },
                      {
                          relation :"SearchedLevel"
                      },
                      {
                        relation:"Comments"
                      }
                    ]
                  }
                },
                {
                  relation : "ParticipatedActivities",
                  scope : {
                    where : {
                      isPublic : options.accessToken.userId == memberId ? {inq : [true,false]} : true,
                    },
                    include:[
                      {
                          relation:'Category',
                          scope:{
                              include : {
                                  relation : "Cover"
                              }
                          }
                      },
                      {
                        relation : "Participants",
                        scope : {
                          where : {
                            invitationStateId : 2
                          }
                        }
                      },
                      {
                          relation : "Address",
                      },
                      {
                          relation:"Creator",
                          scope : {
                              include : [
                                  {
                                      relation : "Avatar"
                                  }
                              ]
                          }
                      },
                      {
                          relation :"SearchedLevel"
                      },
                      {
                        relation:"Comments"
                      }
                    ]
                  }
                }
            ]
        },function(err,profileResult){
            if(err){
                callback(err);
                return;
            }

            if(profileResult == null){
                callbackWithError("MemberNotExists",404,callback);
                return;
            }

            profileResult = JSON.parse(JSON.stringify(profileResult));

            if(profileResult.Avatar){
                profileResult.avatarURL = "storage/"+profileResult.Avatar.container+"/download/" + profileResult.Avatar.name;
            }

            if(profileResult.Cover){
                profileResult.coverURL = "storage/"+profileResult.Cover.container+"/download/" + profileResult.Cover.name;
            }

            profileResult.Activities.forEach(activityResult => {
              if(activityResult && activityResult.Category &&  activityResult.Category.Cover){
                  activityResult.Category.coverURL = "storage/"+activityResult.Category.Cover.container+"/download/" + activityResult.Category.Cover.name;
              }

              if(activityResult && activityResult.Creator &&  activityResult.Creator.Avatar){
                  activityResult.Creator.avatarURL = "storage/"+activityResult.Creator.Avatar.container+"/download/" + activityResult.Creator.Avatar.name;
              }
            });

            var moment = require('moment');

            const now = moment().format("YYYY-MM-DD");

            profileResult.ParticipatedActivities = profileResult.ParticipatedActivities.filter(activity => activity.Participants.find(participant => participant.invitedUserId == memberId) != null);

            profileResult.activitiesCount = profileResult.Activities.length;

            profileResult.Activities = profileResult.Activities.concat(profileResult.ParticipatedActivities);

            profileResult.CurrentActivities = profileResult.Activities.filter(activity => moment(now).isSameOrBefore(activity.startAt));
            profileResult.ActivitiesHistory = profileResult.Activities.filter(activity => moment(now).isAfter(activity.startAt))
            delete profileResult.Activities;

            profileResult.CurrentActivities.sort(compare);
            profileResult.ActivitiesHistory.sort(compare);

            profileResult.followersCount = profileResult.Followers.length;
            delete profileResult.Followers;
            delete profileResult.ParticipatedActivities;

            callback(null,profileResult);
        });
    }

    Profile.myprofile = function (options,withoutEmailVerification,callback) {
        Profile.memberProfile(options,options.accessToken.userId,function(err,profileResult){
            if(err){
                callback(err);
                return;
            }


            if(!withoutEmailVerification && profileResult.User && !profileResult.User.emailVerified){
              callbackWithError(401,"Email not verified",callback)
              return;
            }

            const Category  = Profile.app.models.Category;
            const Notification = Profile.app.models.Notification;

            Category.allCategories(options,function(err,categoriesResult){
              if(err){
                callback(err);
                return;
              }

              Notification.notSeenNotificationsCount(options,function(err,notifResult){
                if(err){
                  callback(err);
                  return;
                }

                profileResult.Categories = categoriesResult;
                profileResult.notifications = notifResult.notifications;

                const BlockedUser = Profile.app.models.BlockedUser;
                const InvitationRequest = Profile.app.models.InvitationRequest;

                InvitationRequest.myPendingInvitations(options,function(err,pendingRequests){
                  if(err){
                    callback(err);
                    return;
                  }

                    profileResult.pendingInvitationsCount = pendingRequests.length;
                    
                    BlockedUser.blockedUsers(options,function(err,blockedUsersResult){
                      if(err){
                          callback(err);
                          return;
                      }

                      profileResult.blockedUsersIds = blockedUsersResult.map(blockedUser => {return blockedUser.userId == options.accessToken.userId ? blockedUser.userToBlockId : blockedUser.userId})
                      
                      console.log("blockedUsersIds === ",profileResult.blockedUsersIds);
                      
                      callback(null,profileResult);

                  });
                });
              });
            });
        });
    }

    Profile.changeAvatar = function (ctx, options, callback) {
        //UPLOAD Attachements
        ctx.req.params.container = 'profile';
        var asset = Profile.app.models.Asset;
        
        asset.uploadMultiple(ctx, options, function (err, uploadResult, fileObj) {
          if (err) {
            console.log(err)
            callback(err);
          } else {

            console.log("entered");
            Profile.findOne({
              where: {
                userId: options.accessToken.userId
              }
            }, function (err, result) {
              if (err) {
                callback(err);
                removeAssett(uploadResult);
              } else {
                if (result.avatarId && result.avatarId > 0) {
                  asset.findOne({
                    where: {
                      assetId: result.avatarId
                    }
                  }, function (err, assetResult) {
                    console.log("asset result ===", assetResult);
                    if (assetResult) {
                      removeAsset(assetResult.container, assetResult.name);
                    }
                  });
                }
    
                result.updateAttribute('avatarId', uploadResult[0].assetId, function (err, updateResult) {
                  if (err) {
                    callback(err);
                    removeAssett(uploadResult);
                  } else {
                    Profile.myprofile(options,true, function (err, result) {
                      if (err) {
                        callback(err);
                        return;
                      }
                      callback(null, result);
                    });
                  }
                });
              }
            });
    
          }
        });
    
    }
    
    Profile.changeCover = function (ctx, options, callback) {
        //UPLOAD Attachements
        ctx.req.params.container = 'profile';
        var asset = Profile.app.models.Asset;
        
        asset.uploadMultiple(ctx, options, function (err, uploadResult, fileObj) {
          if (err) {
            console.log(err)
            callback(err);
          } else {
            Profile.findOne({
              where: {
                userId: options.accessToken.userId
              }
            }, function (err, result) {
              if (err) {
                callback(err);
                removeAssett(uploadResult);
              } else {
                if (result.coverId && result.coverId > 0) {
                  asset.findOne({
                    where: {
                      assetId: result.coverId
                    }
                  }, function (err, assetResult) {
                    console.log("asset result ===", assetResult);
                    if (assetResult) {
                      removeAsset(assetResult.container, assetResult.name);
                    }
                  });
                }
    
                result.updateAttribute('coverId', uploadResult[0].assetId, function (err, updateResult) {
                  if (err) {
                    callback(err);
                    removeAssett(uploadResult);
                  } else {
                    Profile.myprofile(options,true, function (err, result) {
                      if (err) {
                        callback(err);
                        return;
                      }
                      callback(null, result);
                    });
                  }
                });
              }
            });
    
          }
        });
    
    }

    Profile.editProfile = function(params,options,callback) {

      const User = Profile.app.models.User;

      Profile.beginTransaction('READ COMMITTED', function (err, tx) {
        Profile.findOne({
          where: {
            userId : options.accessToken.userId
          }
        },function(err,profileResult){
          if(err){
            callback(err);
            tx.rollback(function (error) {});
            return;
          }

          if(profileResult == null){
            callbackWithError("UserNotExists",404,callback);
            tx.rollback(function (error) {});
            return;
          }

          profileResult.updateAttributes(params,{transaction:tx},function(err,updateResult){
            if(err){
              callback(err);
              tx.rollback(function (error) {});
              return;
            }

            // if(params.username){
            //   profileResult.User.get().then(function (user) {
            //     user.updateAttribute("username",params.username,{transaction:tx},function(err,userUpdateResult){
            //       if(err){
            //         callback(err);
            //         tx.rollback(function (error) {});
            //         return;
            //       }

            //       commitChanges();
            //     });
            //   });
            //   return;
            // }

            commitChanges();

            function commitChanges(){
              tx.commit(function (error) {
                Profile.myprofile(options,true,function(err,result){
                  callback(null,result);
                });
              });
            }
          });
        });
      });
    } 
}