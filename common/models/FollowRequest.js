'use strict';

module.exports = function (FollowRequest) {

    function callbackWithError(errorDescription, errorCode, callback) {
        var error = new Error(errorDescription);
        error.status = errorCode;
        callback(error);
    }

    FollowRequest.follow = function(options,userToFollowId,callback){

        const params = {
            followerId : options.accessToken.userId,
            followedId : userToFollowId,
            requestStateId : 1
        }

        FollowRequest.create(params,function(err,result){
            if(err){
                callback(err);
                return;
            }

            const Notification = FollowRequest.app.models.Notification;
            const Profile = FollowRequest.app.models.Profile;
            const User = FollowRequest.app.models.user;
            Profile.findOne({
                where : {
                    userId : options.accessToken.userId
                }
            },function(err,profileResult){
                if(!err){
                    User.findOne({
                        where : {
                            id : userToFollowId
                        }
                    },function(err,userResult){
                        if(!err){
                            Notification.sendNotification([userResult],null,(profileResult.pseudo || profileResult.firstName)+" souhaite te suivre",{userId : options.accessToken.userId,requestStateId : 1},function(err,result){});
                        }
                    })
                }
            });

            callback(null);
        });

    }

    FollowRequest.cancelFollowRequest = function(options,requestId,callback){

        FollowRequest.findOne({
            where : {
                followRequestId : requestId,
                followerId : options.accessToken.userId,
                requestStateId : 1
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            if(!result){
                callbackWithError("NotExists",404,callback);
                return;
            }

            FollowRequest.destroyById(requestId,function(err){
                if(err){
                    callback(err);
                    return;
                }

                callback(null);
            });
        })

    }

    FollowRequest.unfollow = function(options,requestId,callback){
        FollowRequest.findOne({
            where : {
                followRequestId : requestId,
                followerId : options.accessToken.userId,
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            if(!result){
                callbackWithError("NotExists",404,callback);
                return;
            }

            FollowRequest.destroyById(requestId,function(err){
                if(err){
                    callback(err);
                    return;
                }

                callback(null);
            });
        });
    }

    FollowRequest.respondFollowRequest = function(options,requestId,accepted,callback){
        FollowRequest.findOne({
            where : {
                followRequestId : requestId,
                followedId : options.accessToken.userId,
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            if(!result){
                callbackWithError("NotExists",404,callback);
                return;
            }

            if(accepted){
                result.updateAttribute("requestStateId",2,function(err,resulttt){
                    if(err){
                        callback(err);
                        return;
                    }

                    console.log("result ==== ",result.followerId);

                    const Notification = FollowRequest.app.models.Notification;
                    const Profile = FollowRequest.app.models.Profile;
                    const User = FollowRequest.app.models.user;
                    Profile.findOne({
                        where : {
                            userId : options.accessToken.userId
                        }
                    },function(err,profileResult){
                        if(!err){
                            User.findOne({
                                where : {
                                    id : result.followerId
                                }
                            },function(err,userResult){

                                console.log("userResult ==== ",userResult);

                                if(!err){
                                    Notification.sendNotification([userResult],null,(profileResult.pseudo || profileResult.firstName)+" a accepté ta demande de suivi",{userId : options.accessToken.userId},function(err,result){});
                                }
                            })
                        }
                    });

                    callback(null);
                });
            }else{
                FollowRequest.destroyById(requestId,function(err){
                    if(err){
                        callback(err);
                        return;
                    }
    
                    callback(null);
                });
            }
        });
    }

    FollowRequest.myReceivedRequests = function(options,callback){

        FollowRequest.find({
            where : {
                followedId : options.accessToken.userId,
                requestStateId : 1,
            },
            include : {
                relation : "Follower",
                scope : {
                    include : {
                        relation : "Avatar"
                    }
                }
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            result = JSON.parse(JSON.stringify(result));

            result.forEach(request => {
                if(request && request.Follower &&  request.Follower.Avatar){
                    request.Follower.avatarURL = "storage/"+request.Follower.Avatar.container+"/download/" + request.Follower.Avatar.name;
                }
            });

            callback(null,result);

        });
    }

    FollowRequest.myAcceptedRequests = function(options,callback){

        FollowRequest.find({
            where : {
                followerId : options.accessToken.userId,
                requestStateId : 2,
            },
            include : {
                relation : "Followed",
                scope : {
                    include : {
                        relation : "Avatar"
                    }
                }
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            result = JSON.parse(JSON.stringify(result));

            result.forEach(request => {
                if(request && request.Followed &&  request.Followed.Avatar){
                    request.Followed.avatarURL = "storage/"+request.Followed.Avatar.container+"/download/" + request.Followed.Avatar.name;
                }
            });

            callback(null,result);

        });
    }

    FollowRequest.myFollowers = function(options, activityId, callback){
        FollowRequest.find({
            where : {
                followedId : options.accessToken.userId,
                requestStateId : 2,
            },
            include : {
                relation : "Follower",
                scope : {
                    include : {
                        relation : "Avatar"
                    }
                }
            }
        },function(err,result){
            if(err){
                callback(err);
                return;
            }

            result = JSON.parse(JSON.stringify(result));

            result.forEach(request => {
                if(request && request.Follower &&  request.Follower.Avatar){
                    request.Follower.avatarURL = "storage/"+request.Follower.Avatar.container+"/download/" + request.Follower.Avatar.name;
                }
            });

            if(activityId){
                const InvitationRequest = FollowRequest.app.models.InvitationRequest;

                InvitationRequest.find({
                    where : {
                        activityId : activityId,
                        senderUserId : options.accessToken.userId
                    }
                },function(err,invitations){
                    if(err){
                        callback(err);
                        return;
                    }

                    result.forEach(follower => {
                        follower.invited = invitations.find(invitation => invitation.invitedUserId == follower.followerId) != undefined
                    });

                    callback(null,result);
                });

            }else{

                callback(null,result);
            }

        });
    }

}