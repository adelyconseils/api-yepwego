'use strict';

module.exports = function(Storage) {
    // Storage.afterRemote('upload',function(context,upload, next){
    //     console.log('after upload' + JSON.stringify(upload));
    //     let newName = context.req.query;
    //     const {container, name} = upload.result.files.avatar[0];
    //     newName = context.req.accessToken.userId;
    //     // let ext = name.split('.').slice(-1).pop();
    //     let ext = "png";
    //     let newFullName = `${newName}.${ext}`;
    //     let dlStream = Image.downloadStream(container, name);
    //     let ulStream = Image.uploadStream(container, newFullName);
    //     dlStream.pipe(ulStream);
    //     ulStream.on('finish', () => {
    //         Image.removeFile(container, name, (err) => {
    //             let fullPath = "/images/" + container + "/download/" + newFullName;
    //             upload.result.files.avatar[0].url = fullPath;
    //             upload.result.files.avatar[0].name = newFullName;
    //             context.res.send({});
    //         });
    //     });
    // });

    Storage.beforeRemote('**', function(context, user, next) {
        //2015-12-11
        context.res.set('x-ms-version', '2017-11-09');
        next();
      });
};
