-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 05, 2019 at 12:56 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yepwego_db_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `accesstoken`
--

DROP TABLE IF EXISTS `accesstoken`;
CREATE TABLE IF NOT EXISTS `accesstoken` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `ttl` int(11) DEFAULT NULL,
  `scopes` text,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_AccessToken_user_idx` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accesstoken`
--

INSERT INTO `accesstoken` (`id`, `ttl`, `scopes`, `created`, `userId`) VALUES
('44s03r3rJBqFYQQ3PBO45xsiD2hmWZr2FBtPJRpYnsyGBcXQuwflZDJGm72njTGD', 1209600, NULL, '2019-02-23 14:25:45', 1),
('533N0MLF2kKYuELEAbFvkWhWj5GUCzB9xBVmbqqFw6EnKE4qcNY0TNpVlv7kwUrI', 1209600, NULL, '2019-09-19 13:05:50', 1),
('68EiMfJvFaYBrA1Cidjda0UYeAuvwNtGztLBENEtQjv9R2AneBwH13eSKCmO36t5', 1209600, NULL, '2019-09-20 09:25:24', 1),
('AcyfaFNUaCNv9ecW2pkNvxS9H6viKfFQx0gOB1h1rbIq50BdasbHQozC83H6qECq', 1209600, NULL, '2019-09-19 12:23:58', 1),
('d1jTYhAXiNM0R5foXeZhzeophspqiKpIPJfnW4CpHPhzC4RRpNMiaDSDB5GdF1Bl', 1209600, NULL, '2019-09-19 15:52:41', 1),
('d40GThSmnoHDCuBZ92pkhzuHrE5wcWxMYdtHt0goCYvfP4eexZQLl70u77dbZgo3', 1209600, NULL, '2019-02-23 14:26:12', 1),
('DaPiAdE3TslhludUFBBlmJHHm2KYKMtx2iRgkhyHuVHpip8DnxRHZ1vkhnndyItJ', 1209600, NULL, '2019-03-12 10:37:07', 1),
('DemkqNLjPmAAILCi7ReBO3bWfi6JkAiF1JTqT8GHsQHBrcJV85kBX8SI5ACNcjbR', 1209600, NULL, '2019-09-23 10:04:09', 1),
('dUpnba3cgcVDR9PjxyxC4kCbU64NRvSzW2E4JYc71I2TlYdkLfkoKryrkr3QZxJ1', 1209600, NULL, '2019-09-26 13:36:35', 1),
('eVkyCeL0RyjuncYxhHWUdQtc3IFHFg9Qcp3cu5CdJpvZ7DZmfyFQyRPhnQN6zKKx', 1209600, NULL, '2019-03-12 21:01:53', 1),
('fumLwb6U9MGmbTyxrXIkpQPtWxrxMmoGYxYVNfcFXPfAqr1oOJbMU9Irp0htAHfz', 1209600, NULL, '2019-09-27 13:54:14', 1),
('GsB9BqS1s49EKz3ELaltnH73JSBJC43SVf6oVvxduFqFl2TMv7nME8GL8pwyyQE5', 1209600, NULL, '2019-03-12 10:41:55', 1),
('hRzTHlMJy5jZ3hQmPCs1Dmv2rFGILdzsCkOIoiSbc8cxIzX1SCHuG6S9HurI1haj', 1209600, NULL, '2019-09-20 10:02:06', 1),
('IHIFbZsbXJvR6J5bIoB66LiIG5QM6I1lBezo1B0B1KRO476wPSh558etbtSPVAHb', 1209600, NULL, '2019-09-20 09:25:47', 1),
('iJO1JbI77demM9bFiUzQuNBFHKfGjRFe5NMKfhxJSJ41Y62GufiE50pnHXM2WbLc', 1209600, NULL, '2019-09-20 11:28:17', 1),
('jKmF3wd31L6ZkoaWxGw1DX0xCHWA0wKMK1jFuZFut9qrSOnL4g3m4ZcRVdFJXhU8', 1209600, NULL, '2019-09-26 13:36:54', 1),
('JLMYvY037zLs7ggU9HUtaCHSt52wVQ7lVasYNzvl4F7D2OsjH7zPXEFMBRBGrzCE', 1209600, NULL, '2019-09-25 13:15:47', 1),
('kd5fnfIJhuLHkL1TeQFYRi7qWTWFX6Q5krlpTnMZVvxGNcWKJxV5d1fNKeehGmev', 1209600, NULL, '2019-09-26 10:46:04', 1),
('L9MVIS9WNeDOBIsGmVWCj2rpk2ZYuVZo0Gjj1TKgf1asLZmNafGWbzmotO0MlE7C', 1209600, NULL, '2019-09-26 10:37:23', 1),
('nxarYaYF19wM2ttKpgrnp0GJGYl5vguZeH6ajMKd8TD22FpKEDGlw8qmFBrq1mgH', 1209600, NULL, '2019-09-20 09:24:16', 1),
('OACUWJ3Gy7UlFdXA2aU8YGrPkAkGeMCa94h6Bc7rbEU2u5QHMCqhDzUnIdz1FrGQ', 1209600, NULL, '2019-10-01 08:52:11', 1),
('pMQzOEGdRj7Yst2JSvfFd783YqpxJiBW0RJCKv2HPMrwKKzc6eIMF0NmCOAevIW5', 1209600, NULL, '2019-09-19 13:50:50', 1),
('q4gJB34XArWQyLfwmEEa19a8se8Sr8n8r7OLuOQx1BlixDIFbMZPc7jPKqD0Idxk', 1209600, NULL, '2019-09-26 10:51:16', 1),
('q8JPnjciEn9BZyfb7OZnEFiwjMqU9n9x36BQ12cwlEAFsXQDJ5dXgTpxjtYpWrlK', 1209600, NULL, '2019-09-20 07:55:54', 1),
('tbAfJTCNibAd9NpR3FsFGWFPuwljqHa2dfrsSVS1qGmXzNkdcSTUW2zzSagbgjdy', 1209600, NULL, '2019-09-27 15:22:54', 1),
('TedvxmikeoClEaUBkdU3mP0IklUafMaKdUl42b6wceozBPlFf9jJVFlfZ2iF4CQk', 1209600, NULL, '2019-09-26 13:29:25', 1),
('tpmRnSQsLjBEaobGGFh2kjdSuXM4GUQedu9upSKgqT0LkzVDPlzIHcrCsicVHRJQ', 1209600, NULL, '2019-09-19 13:28:57', 1),
('UIFENMdZkGFbR14ukbR8hOeU3viHBMpkM1LhOYNBHyBnGr9pTL6ejHI3bxMuOHAC', 1209600, NULL, '2019-09-27 08:24:58', 1),
('v9qAFZMC0g9pmaE9CvILhtk2ynHpyARLIJLvkYdQqWXBat4SwQwshnq2mH30pidt', 1209600, NULL, '2019-09-20 08:57:26', 1),
('x86nLESCEykmFS0HyuUL892XiUi4IjeEMiOG5XAhw3bxODgAXodYyIiKxyOyDk0Y', 1209600, NULL, '2019-09-20 09:22:46', 1),
('zpVh6AOeZKyYJCUqAjuIlB88sIe4FMPHlkq7hC3BrZvFWfWExdM4IJSXYNXL7kr6', 1209600, NULL, '2019-03-12 10:45:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `acl`
--

DROP TABLE IF EXISTS `acl`;
CREATE TABLE IF NOT EXISTS `acl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(512) DEFAULT NULL,
  `property` varchar(512) DEFAULT NULL,
  `accessType` varchar(512) DEFAULT NULL,
  `permission` varchar(512) DEFAULT NULL,
  `principalType` varchar(512) DEFAULT NULL,
  `principalId` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
CREATE TABLE IF NOT EXISTS `activity` (
  `activityId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `description` longtext CHARACTER SET utf8mb4,
  `isPublic` tinyint(1) NOT NULL DEFAULT '1',
  `addressId` int(11) UNSIGNED NOT NULL,
  `meetingPointId` int(11) UNSIGNED DEFAULT NULL,
  `startAt` date NOT NULL,
  `atTime` time DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `searchedLevel` int(11) DEFAULT NULL,
  `participantsNumber` int(11) NOT NULL,
  `costShared` varchar(255) CHARACTER SET utf8mb4 DEFAULT '0',
  `carpoolNeeded` varchar(255) CHARACTER SET utf8mb4 DEFAULT '0',
  `potNeeded` varchar(255) CHARACTER SET utf8mb4 DEFAULT '0',
  PRIMARY KEY (`activityId`),
  KEY `fk_Activity_Profile1_idx` (`userId`),
  KEY `fk_Activity_Cetegory1_idx` (`categoryId`),
  KEY `fk_Activity_Level1_idx` (`searchedLevel`),
  KEY `fk_activity_address` (`addressId`),
  KEY `fk_activity_MeetingPoint` (`meetingPointId`)
) ENGINE=InnoDB AUTO_INCREMENT=584 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `activitycomment`
--

DROP TABLE IF EXISTS `activitycomment`;
CREATE TABLE IF NOT EXISTS `activitycomment` (
  `activityCommentId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activityId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `comment` varchar(1024) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `commentedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`activityCommentId`),
  KEY `fk_comment_Activity` (`activityId`),
  KEY `fk_comment_user` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `activitypicture`
--

DROP TABLE IF EXISTS `activitypicture`;
CREATE TABLE IF NOT EXISTS `activitypicture` (
  `activityId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  `addedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`activityId`,`assetId`),
  KEY `fk_Activity_has_Asset_Asset1_idx` (`assetId`),
  KEY `fk_Activity_has_Asset_Activity1_idx` (`activityId`),
  KEY `fk_activityPicture_AddedBy` (`addedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `addressId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fullAddress` varchar(1024) NOT NULL DEFAULT '',
  `lattitude` varchar(45) NOT NULL DEFAULT '',
  `longitude` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`addressId`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`addressId`, `fullAddress`, `lattitude`, `longitude`) VALUES
(1, '13 Hamid el Ghazali, Sousse, Tunisie', '35.8223128', '10.6295371'),
(2, 'Tunis, Tunisie', '36.8064948', '10.1815316'),
(3, 'Mövenpick Hotel Gammarth Tunis, Avenue Taieb Mhiri, Marsa, Tunisie', '36.89338499999999', '10.320789'),
(4, '4000 khezama ouest, Tunisia', '35.8403222', '10.6080978'),
(5, 'Résidence Ahlem, Tunis, Tunisia', '36.8613497', '10.2650951'),
(6, 'Pathé Tunis City, Cebalat Ben Ammar, Tunisie', '36.898271', '10.1247324'),
(7, 'Résidence Ahlem, Tunis, Tunisia', '36.8613497', '10.2650951'),
(8, 'Louvre Museum, Rue de Rivoli, Paris, France', '48.8606111', '2.337644'),
(9, '14 Avenue de la Porte Molitor, 75016 Paris, France', '48.8448533', '2.2525388'),
(10, 'Bois de Boulogne, Paris, France', '48.86244019999999', '2.2491727'),
(11, 'Rond-Point Rhin et Danube, Boulogne-Billancourt, France', '48.84066420000001', '2.2288294'),
(12, 'Parc Monceau, Boulevard de Courcelles, Paris, France', '48.8796835', '2.308955'),
(13, 'Prospect Park, Brooklyn, État de New York, États-Unis', '40.6602037', '-73.9689558'),
(14, '1 Avenue du Général Leclerc, 94700 Maisons-Alfort, France', '48.8156872', '2.4206021'),
(15, 'La Clusaz, France', '45.904427', '6.423353'),
(16, 'Bois de Boulogne, Paris, France', '48.86244019999999', '2.2491727'),
(17, 'Rond-Point Rhin et Danube, Boulogne-Billancourt, France', '48.84066420000001', '2.2288294'),
(18, '42 Rue de Bellevue, 92100 Boulogne-Billancourt, France', '48.8374955', '2.230165'),
(19, 'Roland Garros, Avenue de la Porte d\'Auteuil, Paris, France', '48.8476997', '2.2442659'),
(20, 'Résidence Ahlem, Tunis, Tunisie', '36.8613497', '10.2650951'),
(21, 'Résidence Ahlem, Tunis, Tunisie', '36.8613497', '10.2650951'),
(22, 'Résidence Ahlem, Tunis, Tunisie', '36.8613497', '10.2650951'),
(23, 'Résidence Ahlem, Tunis, Tunisie', '36.8613497', '10.2650951'),
(24, 'Résidence Ahlem, Tunis, Tunisie', '36.8613497', '10.2650951'),
(25, 'Lac Nord, Tunis, Tunisie', '36.84522', '10.2726319'),
(26, 'El Menzah 9, Ariana, Tunisie', '36.847088', '10.1516944'),
(27, 'Géant Tunis City, Cebalat Ben Ammar, Tunisie', '36.8995287', '10.1240203'),
(28, 'Riadh Andalous, Ariana, Tunisie', '36.8805752', '10.1717264'),
(29, 'Bizerte, Tunisie', '37.2767579', '9.8641609'),
(30, 'Géant Tunis City, Cebalat Ben Ammar, Tunisie', '36.8995287', '10.1240203'),
(31, 'Résidence Ahlem, Tunis, Tunisie', '36.8613497', '10.2650951'),
(32, 'Résidence Ahlem, Tunis, Tunisie', '36.8613497', '10.2650951'),
(33, '1 Rue Lac Huron, Tunis 1053, Tunisie', '36.8363853', '10.2478194'),
(34, '1 Rue Lac Huron, Tunis 1053, Tunisie', '36.8363853', '10.2478194'),
(35, 'Lac de Tunis, Tunisie', '36.809406', '10.2095525'),
(36, 'El Menzah 6, Ariana, Tunisie', '36.8491088', '10.1661239'),
(37, '4 Avenue Khezama, Sousse, Tunisia', '35.8430038', '10.6179595'),
(38, 'Lac de Tunis, Tunisie', '36.809406', '10.2095525'),
(39, 'El Menzah 6, Ariana, Tunisie', '36.8491088', '10.1661239'),
(40, 'Théâtre de Carthage, Site archéologique de Carthage, Tunisie', '36.8577778', '10.3294444'),
(41, 'Pathé Tunis City, Cebalat Ben Ammar, Tunisie', '36.898271', '10.1247324'),
(42, 'Géant Tunis City, Cebalat Ben Ammar, Tunisie', '36.8995287', '10.1240203'),
(43, 'Pathé Boulogne, Rue le Corbusier, Boulogne-Billancourt, France', '48.8378403', '2.2393156'),
(44, 'Bois de Boulogne, Paris, France', '48.86244019999999', '2.2491727'),
(45, 'Parc Monceau, Boulevard de Courcelles, Paris, France', '48.8796835', '2.308955'),
(46, 'Opéra Garnier, Place de l\'Opéra, Paris, France', '48.8719697', '2.3316014'),
(47, 'UrbanSoccer - Puteaux, Rue de la République, Puteaux, France', '48.8834223', '2.2306665'),
(48, '42 Rue de Bellevue, 92100 Boulogne-Billancourt, France', '48.8374955', '2.230165'),
(49, 'Hôtel Molitor Paris - MGallery, Rue Nungesser et Coli, Paris, France', '48.8449104', '2.2526562'),
(50, '1 Rue Lac Huron, Tunis 1053, Tunisia', '36.8363853', '10.2478194'),
(51, 'Issy-les-Moulineaux, France', '48.8245306', '2.2743419'),
(52, 'Pathé Tunis City, Cebalat Ben Ammar, Tunisie', '36.898271', '10.1247324'),
(53, 'Géant Tunis City, Cebalat Ben Ammar, Tunisie', '36.8995287', '10.1240203'),
(54, 'L\'Opéra, Thionne, France', '46.407767', '3.5806693'),
(55, 'Galerie Lafayette Rooftop, Boulevard Haussmann, Paris, France', '48.87343', '2.331583'),
(56, 'Pathé Boulogne, Rue le Corbusier, Boulogne-Billancourt, France', '48.8378403', '2.2393156'),
(57, '33 Rue Ali Khlifa, Tunis, Tunisie', '36.8444702', '10.1489514'),
(58, 'L\'Atelier des Sens, Rue Sedaine, Paris, France', '48.85674419999999', '2.3749404'),
(59, 'Pathé Boulogne, Rue le Corbusier, Boulogne-Billancourt, France', '48.8378403', '2.2393156'),
(60, 'Aquarium de Paris, Avenue Albert de Mun, Paris, France', '48.862216', '2.290996'),
(61, 'C39, Fouchana, Tunisia', '36.6972311', '10.1729614'),
(62, 'sahloul 4, Tunisia', '35.835269', '10.606819'),
(63, 'Boulevard Yahia IBN Omar, Sousse, Tunisie', '35.8260606', '10.6345896'),
(64, 'Boulevard Yahia IBN Omar, Sousse, Tunisie', '35.8260606', '10.6345896'),
(65, 'sahloul 4, Tunisia', '35.835269', '10.606819'),
(66, 'Sidi Bou Saïd, Site archéologique de Carthage, Tunisie', '36.8698325', '10.3413691'),
(67, 'Blvd Hassen Ayachi, Sousse, Tunisie', '35.8319923', '10.636607'),
(68, 'Pathé Boulogne, Rue le Corbusier, Boulogne-Billancourt, France', '48.8378403', '2.2393156'),
(69, 'Centre Commercial Les Passages, Rue Tony Garnier, Boulogne-Billancourt, France', '48.8366578', '2.2391219'),
(70, 'Pathé Tunis City, Cebalat Ben Ammar, Tunisie', '36.898271', '10.1247324'),
(71, 'Ave Ariana Les Roses, Ariana, Tunisia', '36.8576688', '10.1619555'),
(72, 'Pathé Tunis City, Cebalat Ben Ammar, Tunisie', '36.898271', '10.1247324'),
(73, 'Immeuble Farah, Rue du Lac Huron, Tunis 1053, Tunisie', '36.8364508', '10.2476139'),
(74, '1 Rue Lac Huron, Tunis 1053, Tunisia', '36.8363853', '10.2478194'),
(75, 'Immeuble Farah, Rue du Lac Huron, Tunis 1053, Tunisie', '36.8364508', '10.2476139'),
(76, '1 Rue Lac Huron, Tunis 1053, Tunisie', '36.8363853', '10.2478194'),
(77, 'Sousse, Tunisie', '35.8245029', '10.634584'),
(78, 'Sousse, Tunisie', '35.8245029', '10.634584'),
(79, 'Sousse, Tunisie', '35.8245029', '10.634584'),
(80, '4000 khezama ouest, Tunisia', '35.8403222', '10.6080978'),
(81, 'La Marsa, Tunisie', '36.8891165', '10.3222671'),
(82, 'Pathé Tunis City, Cebalat Ben Ammar, Tunisie', '36.898271', '10.1247324'),
(83, '1 Rue Lac Huron, Tunis 1053, Tunisia', '36.8363853', '10.2478194'),
(84, '4 Avenue Khezama, Sousse, Tunisia', '35.8430038', '10.6179595'),
(85, 'Blvd Hassen Ayachi, Sousse, Tunisie', '35.8319923', '10.636607'),
(86, 'La Marsa, Tunisie', '36.8891165', '10.3222671'),
(87, 'La Marsa, Tunisie', '36.8891165', '10.3222671'),
(88, '8 Avenue Francis Chaveton, 92210 Saint-Cloud, France', '48.8597295', '2.2083676'),
(89, '8 Avenue Francis Chaveton, 92210 Saint-Cloud, France', '48.8597295', '2.2083676');

-- --------------------------------------------------------

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
CREATE TABLE IF NOT EXISTS `asset` (
  `assetId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `container` varchar(45) NOT NULL,
  PRIMARY KEY (`assetId`)
) ENGINE=InnoDB AUTO_INCREMENT=668 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset`
--

INSERT INTO `asset` (`assetId`, `name`, `type`, `container`) VALUES
(29, '1551744249379hh9jjb.png', 'image/png', 'category'),
(30, '1551744279047xtnw24.png', 'image/png', 'category'),
(32, '1551744336975atclp3.png', 'image/png', 'category'),
(33, '1551785152031kjao5o.png', 'image/png', 'category'),
(34, '1551785165441kgcs1m.png', 'image/png', 'category'),
(35, '1551785178614p06b86.png', 'image/png', 'category'),
(36, '1551785192144yl3hfh.png', 'image/png', 'category'),
(63, '1552387544932fdulhv.jpeg', 'image/jpeg', 'category'),
(64, '1552387594636bzm8lm.jpg', 'image/jpeg', 'category'),
(65, '1552387656620yrxilu.jpg', 'image/jpeg', 'category'),
(66, '1552387667579l0pkdg.jpg', 'image/jpeg', 'category'),
(67, '1552387677115i0q0ho.jpg', 'image/jpeg', 'category'),
(68, '1552387707732xiyh5h.jpg', 'image/jpeg', 'category'),
(76, '1552425152323lupqyu.jpg', 'image/jpeg', 'category'),
(77, '1552425414920z4gc50.jpg', 'image/jpeg', 'category'),
(78, '155242550332009f37x.jpg', 'image/jpeg', 'category'),
(79, '1552425665218fgackl.jpg', 'image/jpeg', 'category'),
(80, '1552425823576n8ebfz.jpg', 'image/jpeg', 'category'),
(81, '1552425988628u047wn.jpg', 'image/jpeg', 'category'),
(82, '1552426102333ifynjw.jpg', 'image/jpeg', 'category'),
(83, '1552426298390fsra7b.jpg', 'image/jpeg', 'category'),
(84, '1552426440910nvach0.jpg', 'image/jpeg', 'category'),
(85, '1552426581280slojre.jpg', 'image/jpeg', 'category'),
(86, '1552426768611ag24x7.jpeg', 'image/jpeg', 'category'),
(87, '15524269618658ydvs9.jpg', 'image/jpeg', 'category'),
(88, '1552427271742qssjdz.jpg', 'image/jpeg', 'category'),
(89, '1552427453422uh59ia.jpg', 'image/jpeg', 'category'),
(90, '1552427727132x89roq.jpg', 'image/jpeg', 'category'),
(91, '1552427913318tjoi5u.jpg', 'image/jpeg', 'category'),
(92, '15524280000356yl6aj.jpg', 'image/jpeg', 'category'),
(93, '1552428076039lw3435.jpg', 'image/jpeg', 'category'),
(94, '1552428237322u9x9rz.jpg', 'image/jpeg', 'category'),
(95, '1552428358250a6bxdh.jpg', 'image/jpeg', 'category'),
(96, '15524284611194cj3r6.jpg', 'image/jpeg', 'category'),
(97, '1552428561515g2p269.jpg', 'image/jpeg', 'category'),
(98, '1552428660047r74fgn.jpg', 'image/jpeg', 'category'),
(99, '1552428770820bj9pw9.jpg', 'image/jpeg', 'category'),
(100, '1552428917161e72v9b.jpg', 'image/jpeg', 'category'),
(101, '15524290217180lx7ph.jpg', 'image/jpeg', 'category'),
(102, '1552429249911uq8a3i.jpg', 'image/jpeg', 'category'),
(103, '1552429368121s5faqe.jpg', 'image/jpeg', 'category'),
(104, '1552429484682gk6zj3.jpg', 'image/jpeg', 'category'),
(622, '1568401996238jkwogn.JPG', 'image/jpeg', 'profile'),
(623, '1568402267240tf581h.JPG', 'image/jpeg', 'activity'),
(624, '1568403241652gpai81.HEIC', 'image/jpeg', 'profile'),
(625, '1568441220648h778w4.HEIC', 'image/jpeg', 'activity'),
(626, '1568441228314bofj8j.HEIC', 'image/jpeg', 'activity'),
(628, '15684418020272salmd.jpg', 'image/jpeg', 'profile'),
(629, '1568444018191s4zd8r.HEIC', 'image/jpeg', 'activity'),
(631, '15684646344037v8mwk.HEIC', 'image/jpeg', 'activity'),
(633, '1568464794459utx9sd.PNG', 'image/jpeg', 'profile'),
(634, '1568464842381r0a2si.JPG', 'image/jpeg', 'profile'),
(637, '1568527249784ox1tir.PNG', 'image/jpeg', 'profile'),
(639, '1568528385631op3ca8.PNG', 'image/jpeg', 'profile'),
(641, '1568715596762bt3ptd.jpg', 'image/jpeg', 'profile'),
(642, '1568750802726kck7fi.JPG', 'image/jpeg', 'activity'),
(643, '1568750807070gamsdu.JPG', 'image/jpeg', 'activity'),
(644, '15687508232834tlfld.JPG', 'image/jpeg', 'activity'),
(645, '15687508303008uhs4l.JPG', 'image/jpeg', 'activity'),
(646, '1568751149940l613m3.png', 'image/jpeg', 'activity'),
(647, '1568751162639lsb54p.png', 'image/jpeg', 'activity'),
(648, '1568751171551afoxmm.png', 'image/jpeg', 'activity'),
(649, '1568757922784yxq7vm.jpg', 'image/jpeg', 'profile'),
(650, '156879585302613cu5x.jpeg', 'image/jpeg', 'profile'),
(651, '1568811079896n15kug.JPG', 'image/jpeg', 'profile'),
(653, '1568811693520edhgq8.JPG', 'image/jpeg', 'profile'),
(654, '156888413651001iyo5.HEIC', 'image/jpeg', 'profile'),
(655, '1569000854199xwihxl.HEIC', 'image/jpeg', 'activity'),
(656, '15690966562472m2o69.HEIC', 'image/jpeg', 'profile'),
(657, '15691507438773b49vh.JPG', 'image/jpeg', 'profile'),
(658, '1569150798710w0g3pg.jpg', 'image/jpeg', 'profile'),
(660, '1569332592355icjjhd.jpg', 'image/jpeg', 'profile'),
(661, '15695730219572srjpe.png', 'image/jpeg', 'activity'),
(662, '1569573061760wz2m92.jpg', 'image/jpeg', 'profile'),
(663, '1569573114183m078qi.png', 'image/png', 'profile'),
(665, '15696072302574brkv3.jpg', 'image/jpeg', 'profile'),
(666, '1569607311945l27hcw.image', 'image/jpeg', 'profile'),
(667, '1572605611016lswqyb.jpg', 'image/jpeg', 'profile');

-- --------------------------------------------------------

--
-- Table structure for table `blockeduser`
--

DROP TABLE IF EXISTS `blockeduser`;
CREATE TABLE IF NOT EXISTS `blockeduser` (
  `userId` int(11) NOT NULL,
  `userToBlockId` int(11) NOT NULL,
  PRIMARY KEY (`userId`,`userToBlockId`),
  KEY `fk_blockedUser_UserToBlock` (`userToBlockId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `name_en` varchar(45) DEFAULT NULL,
  `coverId` int(11) NOT NULL,
  `hasLevel` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`categoryId`),
  KEY `fk_Cetegory_Asset1_idx` (`coverId`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categoryId`, `name`, `name_en`, `coverId`, `hasLevel`) VALUES
(4, 'Ski', 'Sking​', 68, 1),
(6, 'Footing', 'Jogging​', 63, 1),
(7, 'Football', 'Soccer​', 64, 1),
(8, 'Snow', 'Snow​', 65, 1),
(9, 'Tennis', 'Tennis ​', 66, 1),
(10, 'VTT', 'Mountain bike​', 67, 1),
(12, 'Randonnée', 'Hiking​', 76, 1),
(13, 'Kayak', 'Kayak​', 77, 1),
(14, 'Rugby', 'Rugby​', 78, 1),
(15, 'Escalade', 'Climbing​', 79, 1),
(16, 'Basketball', 'Basketball​', 80, 1),
(17, 'Volley-ball', 'Volleyball​', 81, 1),
(18, 'Golf', 'Golf​', 82, 1),
(19, 'Squash', 'Squash​', 83, 1),
(20, 'Surf', 'Surfing​', 84, 1),
(21, 'Paddle', 'Paddle​', 85, 1),
(22, 'Parapente', 'Paragliding​', 86, 1),
(23, 'Delta plane', 'Hang-glider', 87, 1),
(24, 'Théâtre', 'Theater​​', 88, 0),
(25, 'Cinéma', 'Cinema​​', 89, 0),
(26, 'Piscine', 'Swimming pool​​', 90, 0),
(27, 'Patinage', 'Ice skating​​', 91, 1),
(28, 'Karting', 'Karting​​', 92, 1),
(29, 'Belote', 'Card game​​', 93, 1),
(30, 'Cours de cuisine', 'Cooking workshop​​', 94, 0),
(31, 'Pêche', 'Fishing​​', 95, 1),
(32, 'Plongée', 'Diving​​', 96, 1),
(33, 'Equitation', 'Horse riding​​', 97, 1),
(34, 'Parc d\'attraction', 'Theme park​​', 98, 0),
(35, 'Musée', 'Museum​​', 99, 0),
(36, 'Zoo', 'Zoo​​', 100, 0),
(37, 'Paintball', 'Paintball​​', 101, 1),
(38, 'Yoga', 'Yoga​​', 102, 0),
(39, 'Accrobranche', 'Ride walking​​', 103, 0),
(40, 'Aquarium', 'Aquarium​​', 104, 0);

-- --------------------------------------------------------

--
-- Table structure for table `conversation`
--

DROP TABLE IF EXISTS `conversation`;
CREATE TABLE IF NOT EXISTS `conversation` (
  `conversationId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `creator` int(11) NOT NULL,
  `participant` int(11) NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `creatorLastSeenMessages` datetime DEFAULT NULL,
  `participantLastSeenMessages` datetime DEFAULT NULL,
  PRIMARY KEY (`conversationId`),
  KEY `fk_conversation_creator` (`creator`),
  KEY `fk_conversation_participant` (`participant`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `conversationmessage`
--

DROP TABLE IF EXISTS `conversationmessage`;
CREATE TABLE IF NOT EXISTS `conversationmessage` (
  `messageId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `conversationId` int(11) UNSIGNED NOT NULL,
  `sender` int(11) NOT NULL,
  `message` text CHARACTER SET utf8mb4 NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`messageId`),
  KEY `fk_message_conversation` (`conversationId`),
  KEY `fk_message_sender` (`sender`)
) ENGINE=InnoDB AUTO_INCREMENT=793 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `followrequest`
--

DROP TABLE IF EXISTS `followrequest`;
CREATE TABLE IF NOT EXISTS `followrequest` (
  `followRequestId` int(11) NOT NULL AUTO_INCREMENT,
  `followerId` int(11) NOT NULL,
  `followedId` int(11) NOT NULL,
  `requestStateId` int(11) NOT NULL DEFAULT '1',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`followRequestId`),
  KEY `fk_FollowRequest_Profile1_idx` (`followerId`),
  KEY `fk_FollowRequest_Profile2_idx` (`followedId`),
  KEY `fk_FollowRequest_RequestState1_idx` (`requestStateId`)
) ENGINE=InnoDB AUTO_INCREMENT=388 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invitationrequest`
--

DROP TABLE IF EXISTS `invitationrequest`;
CREATE TABLE IF NOT EXISTS `invitationrequest` (
  `invitationRequestId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `senderUserId` int(11) NOT NULL,
  `invitedUserId` int(11) NOT NULL,
  `activityId` int(11) NOT NULL,
  `invitationStateId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `isInvitation` tinyint(4) DEFAULT '1',
  `response1` text CHARACTER SET utf8mb4,
  `response2` text CHARACTER SET utf8mb4,
  `response3` text CHARACTER SET utf8mb4,
  `response4` text CHARACTER SET utf8mb4,
  PRIMARY KEY (`invitationRequestId`),
  KEY `fk_invitation_sender` (`senderUserId`),
  KEY `fk_invitation_invited` (`invitedUserId`),
  KEY `fk_invitation_activity` (`activityId`),
  KEY `fk_invitation_state` (`invitationStateId`)
) ENGINE=InnoDB AUTO_INCREMENT=1610 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

DROP TABLE IF EXISTS `level`;
CREATE TABLE IF NOT EXISTS `level` (
  `levelId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`levelId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`levelId`, `title`) VALUES
(1, 'Débutant'),
(2, 'Moyen'),
(3, 'Avancé'),
(4, 'Expert');

-- --------------------------------------------------------

--
-- Table structure for table `participationrequest`
--

DROP TABLE IF EXISTS `participationrequest`;
CREATE TABLE IF NOT EXISTS `participationrequest` (
  `participationRequestId` int(11) NOT NULL AUTO_INCREMENT,
  `participantId` int(11) NOT NULL,
  `activityId` int(11) NOT NULL,
  `requestStateId` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`participationRequestId`),
  KEY `fk_ParticipationRequest_Profile1_idx` (`participantId`),
  KEY `fk_ParticipationRequest_Activity1_idx` (`activityId`),
  KEY `fk_ParticipationRequest_RequestState1_idx` (`requestStateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `paymentId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `stipeId` varchar(45) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`paymentId`),
  KEY `fk_payment_profile` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paymentmethod`
--

DROP TABLE IF EXISTS `paymentmethod`;
CREATE TABLE IF NOT EXISTS `paymentmethod` (
  `paymentMethodId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `cardNumber` varchar(16) NOT NULL DEFAULT '',
  `expiryMonth` int(11) NOT NULL,
  `expiryYear` int(11) NOT NULL,
  PRIMARY KEY (`paymentMethodId`),
  KEY `fk_paymentMethod_profile` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `userId` int(11) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `isMale` tinyint(1) NOT NULL DEFAULT '1',
  `description` varchar(512) DEFAULT NULL,
  `countryCode` varchar(3) DEFAULT NULL,
  `avatarId` int(11) DEFAULT NULL,
  `coverId` int(11) DEFAULT NULL,
  `providerAvatarURL` varchar(512) DEFAULT NULL,
  `pseudo` varchar(512) DEFAULT NULL,
  `lastNotificationsSeenDate` datetime DEFAULT NULL,
  `sponsorshipCode` varchar(45) DEFAULT NULL,
  `sponsoredBy` int(11) DEFAULT NULL,
  `tokensCount` int(11) NOT NULL DEFAULT '3',
  `allowShowBirthdayDate` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userId`),
  KEY `fk_Profile_Asset1_idx` (`avatarId`),
  KEY `fk_Profile_Asset2_idx` (`coverId`),
  KEY `fk_profile_sponsoredBy` (`sponsoredBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profilecontactinfo`
--

DROP TABLE IF EXISTS `profilecontactinfo`;
CREATE TABLE IF NOT EXISTS `profilecontactinfo` (
  `userId` int(11) NOT NULL,
  `facebook` varchar(512) DEFAULT NULL,
  `google` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reportreason`
--

DROP TABLE IF EXISTS `reportreason`;
CREATE TABLE IF NOT EXISTS `reportreason` (
  `reasonId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL DEFAULT '',
  PRIMARY KEY (`reasonId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reportreason`
--

INSERT INTO `reportreason` (`reasonId`, `title`) VALUES
(1, 'Usurpation d\'identité'),
(2, 'Partage de contenus inappropriés'),
(3, 'Discours incitant à la haine');

-- --------------------------------------------------------

--
-- Table structure for table `reportuser`
--

DROP TABLE IF EXISTS `reportuser`;
CREATE TABLE IF NOT EXISTS `reportuser` (
  `reportUserId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `reasonId` int(11) UNSIGNED DEFAULT NULL,
  `description` text,
  `userId` int(11) NOT NULL,
  `userReportedId` int(11) NOT NULL,
  PRIMARY KEY (`reportUserId`),
  KEY `fk_report_reason` (`reasonId`),
  KEY `fk_report_user` (`userId`),
  KEY `fk_report_reportedUser` (`userReportedId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `requeststate`
--

DROP TABLE IF EXISTS `requeststate`;
CREATE TABLE IF NOT EXISTS `requeststate` (
  `requestStateId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  PRIMARY KEY (`requestStateId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `requeststate`
--

INSERT INTO `requeststate` (`requestStateId`, `title`) VALUES
(1, 'pending'),
(2, 'accepted'),
(3, 'refused'),
(4, 'invited'),
(5, 'requested');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `description`, `created`, `modified`) VALUES
(1, 'ADMIN', 'YepWeGo adminstrator', '2019-02-23 12:31:44', '2019-02-23 12:31:44'),
(2, 'MEMBER', 'YepWeGo user', '2019-02-23 12:33:09', '2019-02-23 12:33:09');

-- --------------------------------------------------------

--
-- Table structure for table `rolemapping`
--

DROP TABLE IF EXISTS `rolemapping`;
CREATE TABLE IF NOT EXISTS `rolemapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `principalType` varchar(45) NOT NULL,
  `principalId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_RoleMapping_user1_idx` (`principalId`),
  KEY `fk_RoleMapping_Role1_idx` (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rolemapping`
--

INSERT INTO `rolemapping` (`id`, `principalType`, `principalId`, `roleId`) VALUES
(1, 'USER', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `selectedcategory`
--

DROP TABLE IF EXISTS `selectedcategory`;
CREATE TABLE IF NOT EXISTS `selectedcategory` (
  `userId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `levelId` int(11) DEFAULT NULL,
  PRIMARY KEY (`userId`,`categoryId`),
  KEY `fk_SelectedCategory_Category` (`categoryId`),
  KEY `fk_SelectedCategory_levelId` (`levelId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `realm` varchar(512) DEFAULT NULL,
  `username` varchar(512) DEFAULT NULL,
  `password` varchar(512) NOT NULL,
  `email` varchar(512) DEFAULT '',
  `emailVerified` tinyint(1) NOT NULL DEFAULT '0',
  `verificationToken` varchar(512) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `provider` int(3) DEFAULT NULL,
  `fcmToken` varchar(512) DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=374 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `realm`, `username`, `password`, `email`, `emailVerified`, `verificationToken`, `createdAt`, `provider`, `fcmToken`, `lastLogin`) VALUES
(1, NULL, 'admin', '$2a$10$mpYyzQRDDa3uhWlalrMOkOd/f8AMw0fIApkvpumtKIKK.RrSog3zG', 'admin@yepwego.com', 0, NULL, '2019-02-23 13:32:19', NULL, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accesstoken`
--
ALTER TABLE `accesstoken`
  ADD CONSTRAINT `fk_accessToken_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `activity`
--
ALTER TABLE `activity`
  ADD CONSTRAINT `fk_Activity_Category` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Activity_Level` FOREIGN KEY (`searchedLevel`) REFERENCES `level` (`levelId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Activity_Profile` FOREIGN KEY (`userId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_activity_MeetingPoint` FOREIGN KEY (`meetingPointId`) REFERENCES `address` (`addressId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_activity_address` FOREIGN KEY (`addressId`) REFERENCES `address` (`addressId`) ON UPDATE CASCADE;

--
-- Constraints for table `activitycomment`
--
ALTER TABLE `activitycomment`
  ADD CONSTRAINT `fk_comment_Activity` FOREIGN KEY (`activityId`) REFERENCES `activity` (`activityId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_comment_user` FOREIGN KEY (`userId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `activitypicture`
--
ALTER TABLE `activitypicture`
  ADD CONSTRAINT `fk_activityPicture_AddedBy` FOREIGN KEY (`addedBy`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_activitypicture_activity` FOREIGN KEY (`activityId`) REFERENCES `activity` (`activityId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_activitypicture_asset` FOREIGN KEY (`assetId`) REFERENCES `asset` (`assetId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blockeduser`
--
ALTER TABLE `blockeduser`
  ADD CONSTRAINT `fk_blockedUser_UserToBlock` FOREIGN KEY (`userToBlockId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_blockedUser_user` FOREIGN KEY (`userId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `fk_category_asset` FOREIGN KEY (`coverId`) REFERENCES `asset` (`assetId`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `conversation`
--
ALTER TABLE `conversation`
  ADD CONSTRAINT `fk_conversation_creator` FOREIGN KEY (`creator`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_conversation_participant` FOREIGN KEY (`participant`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `conversationmessage`
--
ALTER TABLE `conversationmessage`
  ADD CONSTRAINT `fk_message_conversation` FOREIGN KEY (`conversationId`) REFERENCES `conversation` (`conversationId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_message_sender` FOREIGN KEY (`sender`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `followrequest`
--
ALTER TABLE `followrequest`
  ADD CONSTRAINT `fk_FollowRequest_Profile` FOREIGN KEY (`followerId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_FollowRequest_Profile1` FOREIGN KEY (`followedId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_FollowRequest_RequestState` FOREIGN KEY (`requestStateId`) REFERENCES `requeststate` (`requestStateId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invitationrequest`
--
ALTER TABLE `invitationrequest`
  ADD CONSTRAINT `fk_invitation_activity` FOREIGN KEY (`activityId`) REFERENCES `activity` (`activityId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_invitation_invited` FOREIGN KEY (`invitedUserId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_invitation_sender` FOREIGN KEY (`senderUserId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_invitation_state` FOREIGN KEY (`invitationStateId`) REFERENCES `requeststate` (`requestStateId`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `participationrequest`
--
ALTER TABLE `participationrequest`
  ADD CONSTRAINT `fk_ParticipantRequest_Activity` FOREIGN KEY (`activityId`) REFERENCES `activity` (`activityId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ParticipantRequest_Profile` FOREIGN KEY (`participantId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ParticipantRequest_RequestState` FOREIGN KEY (`participationRequestId`) REFERENCES `requeststate` (`requestStateId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `fk_payment_profile` FOREIGN KEY (`userId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `paymentmethod`
--
ALTER TABLE `paymentmethod`
  ADD CONSTRAINT `fk_paymentMethod_profile` FOREIGN KEY (`userId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_Profile_user1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_profile_avatar` FOREIGN KEY (`avatarId`) REFERENCES `asset` (`assetId`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_profile_cover` FOREIGN KEY (`coverId`) REFERENCES `asset` (`assetId`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_profile_sponsoredBy` FOREIGN KEY (`sponsoredBy`) REFERENCES `profile` (`userId`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `profilecontactinfo`
--
ALTER TABLE `profilecontactinfo`
  ADD CONSTRAINT `fk_ProfileContactInfos_Profile` FOREIGN KEY (`userId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reportuser`
--
ALTER TABLE `reportuser`
  ADD CONSTRAINT `fk_report_reason` FOREIGN KEY (`reasonId`) REFERENCES `reportreason` (`reasonId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_report_reportedUser` FOREIGN KEY (`userReportedId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_report_user` FOREIGN KEY (`userId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rolemapping`
--
ALTER TABLE `rolemapping`
  ADD CONSTRAINT `fk_RoleMapping_Role` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_RoleMapping_user1` FOREIGN KEY (`principalId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `selectedcategory`
--
ALTER TABLE `selectedcategory`
  ADD CONSTRAINT `fk_SelectedCategory_Category` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_SelectedCategory_levelId` FOREIGN KEY (`levelId`) REFERENCES `level` (`levelId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_selectedCategory_Profile` FOREIGN KEY (`userId`) REFERENCES `profile` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
